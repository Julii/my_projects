import cv2
import time

#продолжительность отснятого видео в секундах
capture_duration = 4

cam = cv2.VideoCapture('Nature.avi')
cam.get(cv2.CAP_PROP_FRAME_WIDTH)
#для записи видео применяем кодек
fourcc = cv2.VideoWriter_fourcc(*'XVID')
# создаем переменную на выход видео
out = cv2.VideoWriter('video_record.avi', fourcc, 20.0, (1920,1080),0)

print(cam.get(cv2.CAP_PROP_FRAME_WIDTH)) #show frame width
print(cam.get(cv2.CAP_PROP_FRAME_HEIGHT))
print(cam.isOpened())

start_time = time.time()
end_time = time.time()
while (end_time - start_time < capture_duration):
    #завхват по кадрам
    ret,frame = cam.read()
    #меняем видео на чб
    gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
    gray = cv2.flip(gray, 0)
    #записываем видео
    out.write(gray)
    end_time = time.time()
cam.release()
out.release()
cv2.destroyAllWindows
