import cv2
from matplotlib import pyplot

image = cv2.imread('picture.jpg', 0)
#изображение и его верхние и нижние края
edges = cv2.Canny(image, 100,255)

pyplot.subplot(121), pyplot.imshow(image, cmap = 'gray')
pyplot.title('Original image'), pyplot.xticks([]), pyplot.yticks([])
pyplot.subplot(122), pyplot.imshow(edges, cmap = 'gray')
pyplot.title('Edge Image'), pyplot.xticks([]), pyplot.yticks([])

pyplot.show()