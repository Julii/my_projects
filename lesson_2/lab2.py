'''загружаем бибилиотеку opencv'''
import cv2
print(cv2.__version__)

'''загружаем изображение и отображаем его'''
img1 = cv2.imread('pic.jpg', 0)
img2 = cv2.imread('pic.jpg', 1)
print(img1)
cv2.imshow('image', img1)


k = cv2.waitKey(0)
if k == 27:
    cv2.destroyAllWindows()
elif k == ord('q'):
    cv2.imwrite('pic_copy.jpg', img1)
    cv2.imwrite('pic_copy1.png', img2)
    cv2.destroyAllWindows()
