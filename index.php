<?php
ini_set('error_reporting', E_ALL);
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
session_start();
//Connecting to DB with new Class
$DB = new mysqli("localhost", "u0475576_default", "u32a!Ghz", "u0475576_default");
$DB->set_charset("utf8");
//Подключаю файл конфиг для подключения к БД и библиотеку с функциями
$dir = __DIR__ ."/model_view";
$dh  = opendir($dir);
while ($filename = readdir($dh)) { 
	if($filename != '..' && $filename != '.')
		include_once($dir.'/'.$filename); 
}	
// Назначаем модуль и действие по умолчанию.
$module = 'pages';
$id = '1';
if(isset($_SESSION["lang"]))
{
	$lang=$_SESSION["lang"];
}
else
{
	$lang = 'ru';
}
$section_cur_id = 0;
// Массив параметров из URI запроса.
$params = array();
// Если запрошен любой URI, отличный от корня сайта.
if ($_SERVER['REQUEST_URI'] != '/') {
	try {
		// Для того, что бы через виртуальные адреса можно было также передавать параметры
		// через QUERY_STRING (т.е. через "знак вопроса" - ?param=value),
		// необходимо получить компонент пути - path без QUERY_STRING.
		// Данные, переданные через QUERY_STRING, также как и раньше будут содержаться в 
		// суперглобальных массивах $_GET и $_REQUEST.
		$url_path = parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH);
		// Разбиваем виртуальный URL по символу "/"
		$uri_parts = explode('/', trim($url_path, ' /'));
		// Если количество частей не кратно 2, значит, в URL присутствует ошибка и такой URL
		// обрабатывать не нужно - кидаем исключение, что бы назначить в блоке catch модуль и действие,
		// отвечающие за показ 404 страницы.
		/*if (count($uri_parts) % 2) {
			throw new Exception(); 
		}
        */
		if(count($uri_parts) == 3)
        {
            $module = array_shift($uri_parts);
            $action = array_shift($uri_parts);
            $id = array_shift($uri_parts);
        }
        else if(count($uri_parts) == 2)
        {
            $module = array_shift($uri_parts);
            $id = array_shift($uri_parts);
        }
        else if(count($uri_parts) == 1)
        {
            $name = array_shift($uri_parts);
				$id = CMS::page_name_to_id($name);
				if($id!=-1)
				{
					$module = 'pages';
				}
				else
				{
					$id = CMS::section_name_to_id($name);
					if($id!=-1)
					{
						$module = 'sections';
					}
					else
					{
						$id = CMS::partner_name_to_id($name);
						if($id!=-1)
						{
							$module = 'partner';
						}
						else
						{
							$module = '404';
						}
					}
				}
        }
		// Получили в $params параметры запроса
		for ($i=0; $i < count($uri_parts); $i++) {
			$params[$uri_parts[$i]] = $uri_parts[++$i];
		}
	} catch (Exception $e) {
		$module = '404';
		$id = '';
        $lang = 'ru';
	}
}
else
{
    //header("Location:/ru/");
    $id = 1;
    $module = 'pages';
}
if ($module == 'pages')
{ 
	Pages::RenderById($id);
}
else if($module == 'sections')
{ 
    Sections::RenderById($id);
}
else if($module == 'partner')
{ 
    Partners::RenderById($id);
}
else if($module == 'product-page')
{
    $content = Products::GetProductPage($id);
}
else if($module == 'news')
{
    $content = News::GetNewsPage($id);
}
else if($module == 'events')
{
    $content = Events::GetEventsPage($id);
}
else if($module == 'cooperation')
{
    $content = Cooperation::GetCooperationPage($id);
}
else if($module == 'partners')
{
    $content = Partners::GetPartnersPage($id);
}
else if($module == 'about_company')
{
    $content = Company::GetCompanyPage($id);
}
else if($module == 'language')
{
	if($action == 'set')
	{
		$_SESSION['lang'] = $id;
		header("Location:/");
	}
}
else if($module == '404')
{
    $content = Pages::get_404($id);
}
?>


