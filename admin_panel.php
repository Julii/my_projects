<?
#####################################################################
//Модуль страниц сайта
#####################################################################
class Admin
{
    static function get_list()
    {
        global $DB;
        $temp = "<button type='button' class='btn btn-success'><i class='fa fa-plus-circle' aria-hidden='true' style='margin-right:15px;'></i><a href='/admin/?module=pages&action=add'>Добавить страницу</a></button>
		<table class='table table-bordered'>
        <thead class='thead-dark'>
        <tr>
        <th>id страницы</th>
        <th>Заголовок страниц</th>
        <th>Title страницы</th>
        <th>Статус публикации на странице</th>
        </tr>
        </thead>";
        $sql = $DB->prepare("SELECT id, ru_title as title, ru_menu_name as menu_name, status FROM pages");
        $sql->execute();
        $result = $sql->get_result();
        while ($row = $result->fetch_assoc()) 
        {
			//изменение стиля в зависимости от параметра
			$class = '';
			if($row['status'] == 1)
			{
				$class="admin_published";
			}
			else{
				$class="admin_published1";
			}
			
            if($row['status'] == 1)
            {
                $row['status'] = 'Опубликована';
            }
            else
            {
                $row['status'] = 'Не опубликована';
            }
			
            $temp .= "<tr class='".$class."' onclick='window.location.href=`/admin/?module=pages&action=edit&id=$row[id]`; return false'>
						<td>$row[id]</td>
						<td>$row[menu_name]</td>
						<td>$row[title]</td>
                        <td>$row[status]</td>
					</tr>";
        }
        $sql->close();
        $temp .= "</table>";
        return $temp;
		return "";
    }
    
    static function add_form()
    {
        return "<div class='forma'>
        <form action='/admin/?module=pages&action=add' method='POST'>
        <h3>Добавление новой страницы</h3><br />
		<h4><b>Страница на русском языке</b></h4><br />
         <p><b>Название страницы:</b>
			<input type='text' name='ru_menu_name' class='form-control' aria-describedby='sizing-addon2'>
		</p>&nbsp;
		<p><b>Title:</b> 
			<input type='text' name='ru_title' placeholder='Введите заголовок страницы...' class='form-control' aria-describedby='sizing-addon2'>
		</p>
       
        <div class='block'>
            <p><b>Keywords:</b>
				<input type='text' name='ru_keywords' placeholder='Введите ключевые слова...' class='form-control' aria-describedby='sizing-addon2'>
			</p>
            <p><b>Description:</b>
				<input type='text' name='ru_description' placeholder='Введите описание страницы...' class='form-control' aria-describedby='sizing-addon2'>
			</p>&nbsp;
        </div>
        <p><b>Текст страницы:</b><p><textarea id='ru_content_editor' name='ru_content' cols='100' rows='20'></textarea></p>
        <script type='text/javascript'>
            var ckeditor1 = CKEDITOR.replace( 'ru_content_editor' );
            AjexFileManager.init({
                returnTo: 'ckeditor',
                editor: ckeditor1
            });
        </script></p>
        
        <br><h4><b>Страница на английском языке</b></h4>
        <p>Title: 
			<input type='text' name='en_title' class='form-control' aria-describedby='sizing-addon2'>
		</p>
        <p>Name in menu: 
			<input type='text' name='en_menu_name' class='form-control' aria-describedby='sizing-addon2'>
		</p>
        <div class='block'>
            <p>Keywords: 
				<input type='text' name='en_keywords' class='form-control' aria-describedby='sizing-addon2'>
			</p>
            <p>Description: 
				<input type='text' name='en_description' class='form-control' aria-describedby='sizing-addon2'>
			</p>
        </div>
        <p>Text: <textarea id='en_content_editor' name='en_content' cols='100' rows='20'></textarea>
        <script type='text/javascript'>
            var ckeditor1 = CKEDITOR.replace( 'en_content_editor' );
            AjexFileManager.init({
                returnTo: 'ckeditor',
                editor: ckeditor2
            });
        </script></p>&nbsp;
        <p>Опубликовать: <input type='checkbox' name='status' value='1'></p>
            <p>Порядок распопложения в меню: <input type='text' name='number' class='form-control' aria-describedby='sizing-addon2'></p>
		<button class='btn btn-secondary' type='submit' value='Добавить' name='submit' onClick= 'return window.confirm(`Добавить новую страницу?`);'>Добавить</button>

	   
        </form>
        </div>";
    }
    
    static function add_submit()
    {
        $ru_title = $_POST['ru_title'];
        $ru_menu_name = $_POST['ru_menu_name'];
        $ru_keywords = $_POST['ru_keywords'];
        $ru_description = $_POST['ru_description'];
        $ru_content = html_entity_decode($_POST['ru_content']);
        
        $en_title = $_POST['en_title'];
        $en_menu_name = $_POST['en_menu_name'];
        $en_keywords = $_POST['en_keywords'];
        $en_description = $_POST['en_description'];
        $en_content = html_entity_decode($_POST['en_content']);
        
        $ru_url = CMS::translit($ru_menu_name);
        $en_url = CMS::translit($en_menu_name);
        
        $status = $_POST['status'];
        $priority = $_POST['priority'];
        
        global $DB;
        $sql = $DB->prepare("INSERT INTO pages (ru_title, ru_menu_name, ru_keywords, ru_description, ru_content, en_title, en_menu_name, en_keywords, en_description, en_content, status, priority, ru_url, en_url) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
        $sql->bind_param('ssssssssssssss', $ru_title, $ru_menu_name, $ru_keywords, $ru_description, $ru_content, $en_title, $en_menu_name, $en_keywords, $en_description, $en_content, $status, $priority, $ru_url, $en_url);
        $sql->execute();
        $result = $sql->get_result();
        header("Location:/admin/?module=pages");
    }
        
    static function edit_form($id)
    {
        global $DB;
        $temp = "";
        $sql = $DB->prepare("SELECT * FROM pages WHERE id=?");
        $sql->bind_param('s', $id);
        $sql->execute();
        $result = $sql->get_result();
        while ($row = $result->fetch_assoc()) 
        {
            $id = $row['id'];
            
            $ru_title = $row['ru_title'];
            $ru_menu_name = $row['ru_menu_name'];
            $ru_keywords = $row['ru_keywords'];
            $ru_description = $row['ru_description'];
            $ru_content = html_entity_decode($row['ru_content']);
            
            $en_menu_name = $row['en_menu_name'];
			$en_title = $row['en_title'];
            $en_keywords = $row['en_keywords'];
            $en_description = $row['en_description'];
            $en_content = html_entity_decode($row['en_content']);
            $status = $row['status'];
            $priority = $row['priority'];
        }
        $temp = "<div class='forma'>
        <form action='/admin/?module=pages&action=edit' method='POST'>
        <input type='hidden' name='id' value='$id'>
		  <button type='button' class='btn btn-danger'><a href='/admin/?module=pages&action=delete&id=$id' onClick= 'return window.confirm(`Вы уверены, что хотите удалить страницу?`);'><span>Удалить страницу</span></a></button>
        <h3>Редактирование страницы</h2><br />
		<h4><b>Страница на русском языке</b></h4><br />
        <p><b>Название страницы:</b>
			<input type='text' name='ru_menu_name' value='$ru_menu_name' value='$ru_title' class='form-control' aria-describedby='sizing-addon2'>
		</p>&nbsp;
		<p><b>Title:</b> 
			<input type='text' name='ru_title' placeholder='Введите название страницы...' value='$ru_title' class='form-control' aria-describedby='sizing-addon2'>
		</p>
        <div class='block'>
            <p><b>Keywords:</b>
			<input type='text'  name='ru_keywords' placeholder='Введите ключевые слова...' value='$ru_keywords' value='$ru_title' class='form-control' aria-describedby='sizing-addon2'>
			</p>
            <p><b>Description:</b> 
				<input type='text' name='ru_description' placeholder='Введите описание страницы...' value='$ru_description' value='$ru_menu_name' value='$ru_title' class='form-control' aria-describedby='sizing-addon2'>
			</p>&nbsp;
        </div>
        <p>Текст: <p><textarea id='ru_content_editor' name='ru_content' cols='100' rows='20'>$ru_content</textarea></p>
        <script type='text/javascript'>
            var ru_content_editor = CKEDITOR.replace( 'ru_content_editor' );
            AjexFileManager.init({
                returnTo: 'ckeditor',
                editor: ru_content_editor
            });
        </script></p><br />
        
        <h4><b>Страница на английском языке</b></h4>
        <p>Title: 
			<input type='text' name='en_title' value='$en_title' class='form-control' aria-describedby='sizing-addon2'>
		</p>
        <p>Name in menu: 
			<input type='text' name='en_menu_name' value='$en_menu_name' class='form-control' aria-describedby='sizing-addon2'>
		</p>
        <div class='block'>
            <p>Keywords: 
				<input type='text' name='en_keywords' value='$en_keywords' class='form-control' aria-describedby='sizing-addon2'>
			</p>
            <p>Description:
				<input type='text' name='en_description' value='$en_description' class='form-control' aria-describedby='sizing-addon2'>
			</p>
        </div>
        <p>Text: <textarea id='en_content_editor' name='en_content' cols='100' rows='20'>$en_content</textarea>
        <script type='text/javascript'>
            var en_content_editor = CKEDITOR.replace( 'en_content_editor' );
            AjexFileManager.init({
                returnTo: 'ckeditor',
                editor: en_content_editor
            });
        </script></p>&nbsp;";
        if($status =='1')
        {
            $temp .= "<p>Опубликовать: <input type='checkbox' name='status' value='1' checked></p>";
        }
        else
        {
            $temp .= "<p>Опубликована: <input type='checkbox' name='status' value='1'></p>";
        }
        $temp .= "<p>Порядок расположения в меню: <input type='text' type='number' name='priority' value='$priority' class='form-control' aria-describedby='sizing-addon2'></p>
		<button class='btn btn-secondary' type='submit' value='Изменить' name='submit' onClick= 'return window.confirm(`Вы уверены, что хотите применить изменения к этой странице?`);'>Изменить</button>
        </form>
        </div>";
        return $temp;
    }
    
    static function edit_submit()
    {
        $id = $_POST['id'];
            
        $ru_title = $_POST['ru_title'];
        $ru_menu_name = $_POST['ru_menu_name'];
        $ru_keywords = $_POST['ru_keywords'];
        $ru_description = $_POST['ru_description'];
        $ru_content = html_entity_decode($_POST['ru_content']);
        
        $en_title = $_POST['en_title'];
        $en_menu_name = $_POST['en_menu_name'];
        $en_keywords = $_POST['en_keywords'];
        $en_description = $_POST['en_description'];
        $en_content = html_entity_decode($_POST['en_content']);
        
        $status = $_POST['status'];
        $priority = $_POST['priority'];
        
        $ru_url = CMS::translit($_POST['ru_menu_name']);
        $en_url = CMS::translit($_POST['en_menu_name']);
       
        global $DB;
        $sql = $DB->prepare("UPDATE pages SET ru_title=?, ru_menu_name=?, ru_keywords=?, ru_description=?, ru_content=?, en_title=?, en_menu_name=?, en_keywords=?, en_description=?, en_content=?, status=?, priority=?, ru_url=?, en_url=? WHERE id=?");
        $sql->bind_param('sssssssssssssss', $ru_title, $ru_menu_name, $ru_keywords, $ru_description, $ru_content, $en_title, $en_menu_name, $en_keywords, $en_description, $en_content, $status, $priority, $ru_url, $en_url, $id);
        $sql->execute();
        $result = $sql->get_result();
        header("Location:/admin/?module=pages");
    }
    
    function delete($id)
    {
        global $DB;
        $sql = $DB->prepare("DELETE FROM pages WHERE id=?");
        $sql->bind_param('s', $id);
        $sql->execute();
        header("Location:/admin/?module=pages");
    }
}
#####################################################################
//Модуль категории товаров
#####################################################################
class admin_sections
{
    static function get_list()
    {
        global $DB;
        $temp = "<button type='button' class='btn btn-success'><i class='fa fa-plus-circle' aria-hidden='true' style='margin-right:15px;'></i><a href='/admin/?module=sections&action=add'>Добавить категорию</a></button>
        <table class='table table-bordered'>
        <thead>
        <tr>
        <th>id категории</th>
        <th>Наименование</th>
        <th>Статус публикации на странице</th>
        </tr>
        </thead>";
        $sql = $DB->prepare("SELECT id, ru_name as name, status FROM sections");
        $sql->execute();
        $result = $sql->get_result();
        while ($row = $result->fetch_assoc()) 
        {
			$class = '';
			if($row['status'] == 1)
			{
				$class="admin_published";
			}
			else{
				$class="admin_published1";
			}
			
            if($row['status'] == 1)
            {
                $row['status'] = 'Опубликована';
            }
            else
            {
                $row['status'] = 'Не опубликована';
            }
            $temp .= "<tr class='".$class."' onclick='window.location.href=`/admin/?module=sections&action=edit&id=$row[id]`; return false'>
                        <td>$row[id]</td>
                        <td>$row[name]</td>
                        <td>$row[status]</td>
                    </tr>";
        }
        $sql->close();
        $temp .= "</table>";
        return $temp;
		
    }
    
    static function add_form()
    {
        return "<div class='forma'>
        <form action='/admin/?module=sections&action=add' method='POST'>
        <h3>Добавление новой категории товаров</h3><br />
		<h4><b>Страница на русском языке</b></h4>
		<div class='col-md-6 col-xs-12'>
	   <p>Название категории: 
			<input type='text' placeholder='Введите название категории...' name='ru_name' class='form-control' aria-describedby='sizing-addon2'>
		</p> 
		</div>
		<div class='col-md-6 col-xs-12'>
        <div class='block'>
            <p><b>TITLE:</b> 
				<input type='text' placeholder='Введите заголовок ...' name='ru_title' class='form-control' aria-describedby='sizing-addon2'>
			</p>
            <p><b>KEYWORDS: </b>
				<input type='text' placeholder='Введите ключевые слова ...' name='ru_keywords' class='form-control' aria-describedby='sizing-addon2'>
			</p>
            <p><b>DESCRIPTION: </b>
				<input type='text' placeholder='Введите описание ...' name='ru_description' class='form-control' aria-describedby='sizing-addon2'>
			</p>
        </div></div><br /><br />
        
        <br><h4><b>Страница на английском языке</b></h4>
        <div class='col-md-6 col-xs-12'>
		<p>Name of page: 
			<input type='text' placeholder='Write name of category ...' name='en_name' class='form-control' aria-describedby='sizing-addon2'>
		</p>
		</div>
		<div class='col-md-6 col-xs-12'>
        <div class='block'>
            <p><b>Title:  </b>
				<input type='text' placeholder='Write title ...' name='en_title' class='form-control' aria-describedby='sizing-addon2'>
			</p>
            <p><b>Keywords:  </b>
				<input type='text' placeholder='Write keywords ...' name='en_keywords' class='form-control' aria-describedby='sizing-addon2'>
			</p>
            <p><b>Description:  </b>
				<input type='text' placeholder=Write description ...' name='en_description' class='form-control' aria-describedby='sizing-addon2'>
			</p>
        </div></div>&nbsp;
        
        <p>Опубликовать: <input type='checkbox' name='status' value='1'></p>
        <button class='btn btn-secondary' type='submit' value='Добавить' name='submit' onClick= 'return window.confirm(`Добавить новый раздел??`);'>Добавить</button>
        </form>
        </div>";
    }
    
    static function add_submit()
    {
        $ru_title = $_POST['ru_title'];
        $ru_name = $_POST['ru_name'];
        $ru_keywords = $_POST['ru_keywords'];
        $ru_description = $_POST['ru_description'];
        
        $en_title = $_POST['en_title'];
        $en_name = $_POST['en_name'];
        $en_keywords = $_POST['en_keywords'];
        $en_description = $_POST['en_description'];
        
        $status = $_POST['status'];
        
        $ru_url = CMS::translit($ru_name);
        $en_url = CMS::translit($en_name);
        
        global $DB;
        $sql = $DB->prepare("INSERT INTO sections (ru_title, ru_name, ru_keywords, ru_description, en_title, en_name, en_keywords, en_description, status, ru_url, en_url) VALUES (?,?,?,?,?,?,?,?,?,?,?)");
        $sql->bind_param('sssssssssss', $ru_title, $ru_name, $ru_keywords, $ru_description, $en_title, $en_name, $en_keywords, $en_description, $status, $ru_url, $en_url);
        $sql->execute();
        $result = $sql->get_result();
        header("Location:/admin/?module=sections");
    }
        
    static function edit_form($id)
    {
        global $DB;
        $temp = "";
        $sql = $DB->prepare("SELECT * FROM sections WHERE id=?");
        $sql->bind_param('s', $id);
        $sql->execute();
        $result = $sql->get_result();
        while ($row = $result->fetch_assoc()) 
        {
            $id = $row['id'];
            
            $ru_title = $row['ru_title'];
            $ru_name = $row['ru_name'];
            $ru_keywords = $row['ru_keywords'];
            $ru_description = $row['ru_description'];
            
            $en_title = $row['en_title'];
            $en_name = $row['en_name'];
            $en_keywords = $row['en_keywords'];
            $en_description = $row['en_description'];
            
            $status = $row['status'];
            /*$priority = $row['priority'];*/
        }
        $temp = "<div class='forma'>
        <form action='/admin/?module=sections&action=edit' method='POST'>
        <input type='hidden' name='id' value='$id'>
          <button type='button' class='btn btn-danger'><a href='/admin/?module=sections&action=delete&id=$id' onClick= 'return window.confirm(`Вы уверены, что хотите удалить раздел?`);'><span>Удалить раздел</span></a></button>
        <h3>Редактирование страницы</h3><br />
		<h4><b>Страница на русском языке</b></h4>
        <div class='col-md-6 col-md-12'>
		<p>Название категории: 
			<input type='text' name='ru_name' value='$ru_name' class='form-control' aria-describedby='sizing-addon2'>
		</p> 
		</div>
		<div class='col-md-6 col-xs-12'>
        <div class='block'>
            <p><b>TITLE:</b> 
				<input type='text' name='ru_title' value='$ru_title' class='form-control' aria-describedby='sizing-addon2'>
			</p>
            <p><b>KEYWORDS: </b> 
				<input type='text' name='ru_keywords' value='$ru_keywords' class='form-control' aria-describedby='sizing-addon2'>
			</p>
            <p><b>DESCRIPTION:</b>  
				<input type='text' name='ru_description' value='$ru_description' class='form-control' aria-describedby='sizing-addon2'>
			</p>
        </div></div><br />
        
        <h4><b>Страница на английском языке</b></h4>
        <div class='col-md-6 col-xs-12'>
		<p>Name of page: 
			<input type='text' name='en_name' value='$en_name' class='form-control' aria-describedby='sizing-addon2'>
		</p>
		</div>
		<div class='col-md-6 col-xs-12'>
        <div class='block'>
            <p><b>Title: </b> 
				<input type='text' name='en_title' value='$en_title' class='form-control' aria-describedby='sizing-addon2'>
			</p>
            <p><b>Keywords:</b>  
				<input type='text' name='en_keywords' value='$en_keywords' class='form-control' aria-describedby='sizing-addon2'>
			</p>
            <p><b>Description:</b> 
				<input type='text' name='en_description' value='$en_description' class='form-control' aria-describedby='sizing-addon2'>
			</p>
        </div></div>&nbsp";
        if($status =='1')
        {
            $temp .= "<p>Опубликован: <input type='checkbox' name='status' value='1' checked></p>";
        }
        else
        {
            $temp .= "<p>Опубликован: <input type='checkbox' name='status' value='1'></p>";
        }
        $temp .= "<button class='btn btn-secondary' type='submit' value='Изменить' name='submit' onClick= 'return window.confirm(`Изменить раздел??`);'>Изменить</button>
        </form>
        </div>";
        return $temp;
    }
    
    static function edit_submit()
    { 
        $id = $_POST['id'];
            
            $ru_title = $_POST['ru_title'];
            $ru_name = $_POST['ru_name'];
            $ru_keywords = $_POST['ru_keywords'];
            $ru_description = $_POST['ru_description'];
            
            $en_title = $_POST['en_title'];
            $en_name = $_POST['en_name'];
            $en_keywords = $_POST['en_keywords'];
            $en_description = $_POST['en_description'];
              
            $status = $_POST['status'];
            
            $ru_url = CMS::translit($ru_name);
            $en_url = CMS::translit($en_name);
       
        global $DB;
        $sql = $DB->prepare("UPDATE sections SET ru_title=?, ru_name=?, ru_keywords=?, ru_description=?, en_title=?, en_name=?, en_keywords=?, en_description=?, status=?, ru_url=?, en_url=? WHERE id=?");
        $sql->bind_param('ssssssssssss', $ru_title, $ru_name, $ru_keywords, $ru_description, $en_title, $en_name, $en_keywords, $en_description, $status, $ru_url, $en_url, $id);
        $sql->execute();
        $result = $sql->get_result();
        header("Location:/admin/?module=sections");
    }
    
    static function delete($id)
    {
        global $DB;
        $sql = $DB->prepare("DELETE FROM sections WHERE id=?");
        $sql->bind_param('s', $id);
        $sql->execute();
        header("Location:/admin/?module=sections");
    }
}
#####################################################################
//Модуль товаров продукции
#####################################################################
class admin_products
{
    static function get_list()
    {
        global $DB;
        
        $temp = "<button type='button' class='btn btn-success'><i class='fa fa-plus-circle' aria-hidden='true' style='margin-right:15px;'></i><a href='/admin/?module=products&action=add'>Добавить товар</a></button>
        <table class='table table-bordered'>
        <thead>
        <tr>
        <th>id товара</th>
        <th>Наименование</th>
        <th>Категория</th>
        <th>Статус публикации на странице</th>
        </tr>
        </thead>";
        $sql = $DB->prepare("SELECT id, ru_name as name, (SELECT ru_name FROM sections WHERE id=section_id) as section, status FROM products");
        $sql->execute();
        $result = $sql->get_result();
        while ($row = $result->fetch_assoc()) 
        {
			$class = ''; 
			if($row['section'] == 1) 
			{ 
				$class="admin_published"; 
			} 
			else
			{ 
				$class="admin_published1"; 
			}
			if($row['section'] == 2) 
			{ 
				$class="admin_published2";
			}
			else
			{ 
				$class="admin_published1"; 
			}
			if($row['section'] == 3) 
			{ 
				$class="admin_published2"; 
			}
			else
			{ 
				$class="admin_published3"; 
			}
			if($row['section'] == 4) 
			{ 
				$class="admin_published2"; 
			}			
			else
			{ 
				$class="admin_published4"; 
			}
			
            if($row['status'] == 1)
            {
                $row['status'] = 'Опубликован';
            }
            else
            {
                $row['status'] = 'Не опубликован';
            }
            $temp .= "<tr class='".$class."' onclick='window.location.href=`/admin/?module=products&action=edit&id=$row[id]`; return false'>
                        <td>$row[id]</td>
                        <td>$row[name]</td>
                        <td>$row[section]</td>
                        <td>$row[status]</td>
                    </tr>";
        }
        $sql->close();
        $temp .= "</table>";
        return $temp;
		
    }
    
    static function add_form()
    {
        $temp = "<div class='forma'>
        <form action='/admin/?module=products&action=add' method='POST' enctype='multipart/form-data'>
        <h3>Добавление нового товара</h3><br />
		<h4><b>Страница на русском языке</b></h4>
        <div class='col-md-6 col-xs-12'>
		<p>Название товара: 
			<input type='text' placeholder='Введите название товара' name='ru_name' class='form-control' aria-describedby='sizing-addon2'>
		</p>
		<p>Описание о применении: 
			<input type='text' placeholder='Введите название товара' name='ru_about' class='form-control' aria-describedby='sizing-addon2'>
		</p>
		<p>Мини-описание товара: 
				<input type='text' placeholder='Введите название товара...' name='ru_mini_description' class='form-control' aria-describedby='sizing-addon2'>
		</p>
		</div>
		<div class='col-md-6 col-xs-12'>
        <div class='block'>
            <p><b>TITLE: </b>
				<input type='text' placeholder='Введите название...' name='ru_title' class='form-control' aria-describedby='sizing-addon2'>
			</p>
            <p><b>KEYWORDS: </b>
				<input type='text' placeholder='Введите ключевые слова...' name='ru_keywords' class='form-control' aria-describedby='sizing-addon2'>
			</p>
            <p><b>DESCRIPTION: </b>
				<input type='text' placeholder='Введите описание...' name='ru_description' class='form-control' aria-describedby='sizing-addon2'>
			</p>
			</div></div>
			<p>Картинка продукции: 
				<input type='file' name='image' class='form-control' aria-describedby='sizing-addon2' accept='image/*,image/jpeg'>
			</p>
			<p>Первая миниатюра картинки: 
				<input type='file' name='img_preview_1' class='form-control' aria-describedby='sizing-addon2' accept='image/*,image/jpeg'>
			</p>
			<p>Вторая миниатюра картинки: 
				<input type='file' name='img_preview_2' class='form-control' aria-describedby='sizing-addon2' accept='image/*,image/jpeg'>
			</p>
			<p>Третья миниатюра картинки: 
				<input type='file' name='img_preview_3' class='form-control' aria-describedby='sizing-addon2' accept='image/*,image/jpeg'>
			</p>
			<p>Четвертая миниатюра картинки: 
				<input type='file' name='img_preview_4' class='form-control' aria-describedby='sizing-addon2' accept='image/*,image/jpeg'>
			</p>
        <p>Текст страницы: <textarea id='ru_content_editor' name='ru_content' cols='100' rows='20'></textarea>
        <script type='text/javascript'>
            var ckeditor1 = CKEDITOR.replace( 'ru_content_editor' );
            AjexFileManager.init({
                returnTo: 'ckeditor',
                editor: ckeditor1
            });
        </script></p><br />
        
        <br><h4><b>Страница на английском языке</b></h4>
        <div class='col-md-6 col-xs-12'>
		<p>Name of product: 
			<input type='text' placeholder='Write name of production...' name='en_name' class='form-control' aria-describedby='sizing-addon2'>
		</p>
		<p>Description of application: 
			<input type='text' placeholder='Write description of application...' name='en_about' class='form-control' aria-describedby='sizing-addon2'>
		</p>
		<p>Mini-description: 
				<input type='text' placeholder='Write mini-description...' name='en_mini_description' class='form-control' aria-describedby='sizing-addon2'>
		</p>
		</div>
		<div class='col-md-6 col-xs-12'>
        <div class='block'>
            <p><b>Title: </b>
				<input type='text' placeholder='Write title...' name='en_title' class='form-control' aria-describedby='sizing-addon2'>
			</p>
            <p><b>Keywords: </b>
				<input type='text' placeholder='Write keywords...' name='en_keywords' class='form-control' aria-describedby='sizing-addon2'>
			</p>
            <p><b>Description: </b>
				<input type='text' placeholder='Write description...' name='en_description' class='form-control' aria-describedby='sizing-addon2'>
			</p>
        </div>
		</div>
        <p>Text of prodiction: <textarea id='en_content_editor' name='en_content' cols='100' rows='20'></textarea>
        <script type='text/javascript'>
            var ckeditor1 = CKEDITOR.replace( 'en_content_editor' );
            AjexFileManager.init({
                returnTo: 'ckeditor',
                editor: ckeditor1
            });
        </script></p>";
        global $DB;
        $option = "
		&nbsp;<p>Выберите категорию:</p>
		<select name='section_id'>
		<option disabled>Выберите раздел</option>
        <option value='0'>Раздел не задан</option>";
        $sql_option = $DB->prepare("SELECT id, ru_name as name FROM sections");
        $sql_option->execute();
        $result_option = $sql_option->get_result();
        while ($row = $result_option->fetch_assoc()) 
        {
            $option .=  '<option value="'.$row['id'].'">'.$row['name'].'</option>';
        }
        $option .= "</select>";
        $temp.= $option;
        $temp.= "<p>Цена(в рублях): <input type='text' name='price'></p>
                <p>Опубликовать: <input type='checkbox' name='status' value='1'></p>
               <button class='btn btn-secondary' type='submit' value='Добавить' name='submit'>Добавить</button>
                </form>
                </div>";
                return $temp;
    }
    
    static function add_submit()
    {
        $ru_title = $_POST['ru_title'];
        $ru_name = $_POST['ru_name'];
		$ru_about = $_POST['ru_about'];
		$ru_mini_description = $_POST['ru_mini_description'];
        $ru_keywords = $_POST['ru_keywords'];
        $ru_description = $_POST['ru_description'];
        $ru_content = html_entity_decode($_POST['ru_content']);
        
        $en_title = $_POST['en_title'];
        $en_name = $_POST['en_name'];
		$en_about = $_POST['en_about'];
		$en_mini_description = $_POST['en_mini_description'];
        $en_keywords = $_POST['en_keywords'];
        $en_description = $_POST['en_description'];
        $en_content = html_entity_decode($_POST['en_content']);
        
        $status = $_POST['status'];
        $price = $_POST['price'];
        $section_id = $_POST['section_id'];
        
        global $DB;
        $sql = $DB->prepare("INSERT INTO products (ru_title, ru_name, ru_about, ru_mini_description, ru_keywords, ru_description, ru_content, en_title, en_name, en_about,  en_mini_description, en_keywords, en_description, en_content, status, price, section_id) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
        $sql->bind_param('sssssssssssssssss', $ru_title, $ru_name, $ru_about, $ru_mini_description, $ru_keywords, $ru_description, $ru_content, $en_title, $en_name, $en_about, $en_mini_description, $en_keywords, $en_description, $en_content, $status, $price, $section_id);
        $sql->execute();
        $result = $sql->get_result();
		
		//fdddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddd
		if($_FILES["image"]["size"]>0) 
		{
			$uploaddir = '../img-data/uploads/';
			if($_FILES["image"]["error"] == 0)
			{
				if($_FILES['image']['type'] == "image/gif" || $_FILES['image']['type'] == "image/jpg" || $_FILES['image']['type'] == "image/jpeg" || $_FILES['image']['type'] == "image/png") 
				{
					$uploadfile = $uploaddir .time().'_'. basename($_FILES['image']['name']);
					if(move_uploaded_file($_FILES['image']['tmp_name'], $uploadfile)) {
						$inserted_id = $sql->insert_id;
						$sql = $DB->prepare("UPDATE products SET image=? WHERE id=?");
						$uploadfile_real = '/img-data/uploads/' .time().'_'. basename($_FILES['image']['name']);
						$sql->bind_param('si', $uploadfile_real, $inserted_id);
						$sql->execute();
						$result = $sql->get_result();
					}
				}
				else
				{
					$error='Недопустимый формат изображения';
				}
			}
			else {
				$error='Изображение не загружено. Попробуйте ещё раз';
			}	
		}
		
		///////////превью фото 1
		if($_FILES["img_preview_1"]["size"]>0) 
		{
			$uploaddir = '../img-data/uploads/';
			if($_FILES["img_preview_1"]["error"] == 0)
			{
				if($_FILES['img_preview_1']['type'] == "image/gif" || $_FILES['img_preview_1']['type'] == "image/jpg" || $_FILES['img_preview_1']['type'] == "image/jpeg" || $_FILES['img_preview_1']['type'] == "image/png") 
				{
					$uploadfile = $uploaddir .time().'_'. basename($_FILES['img_preview_1']['name']);
					if(move_uploaded_file($_FILES['img_preview_1']['tmp_name'], $uploadfile)) {
						$inserted_id = $sql->insert_id;
						$sql = $DB->prepare("UPDATE products SET img_preview_1=? WHERE id=?");
						$uploadfile_real = '/img-data/uploads/' .time().'_'. basename($_FILES['img_preview_1']['name']);
						$sql->bind_param('si', $uploadfile_real, $inserted_id);
						$sql->execute();
						$result = $sql->get_result();
					}
				}
				else
				{
					$error='Недопустимый формат изображения';
				}
			}
			else {
				$error='Изображение не загружено. Попробуйте ещё раз';
			}	
		}
		
		///////////превью фото 2
		if($_FILES["img_preview_2"]["size"]>0) 
		{
			$uploaddir = '../img-data/uploads/';
			if($_FILES["img_preview_2"]["error"] == 0)
			{
				if($_FILES['img_preview_2']['type'] == "image/gif" || $_FILES['img_preview_2']['type'] == "image/jpg" || $_FILES['img_preview_2']['type'] == "image/jpeg" || $_FILES['img_preview_2']['type'] == "image/png") 
				{
					$uploadfile = $uploaddir .time().'_'. basename($_FILES['img_preview_2']['name']);
					if(move_uploaded_file($_FILES['img_preview_2']['tmp_name'], $uploadfile)) {
						$inserted_id = $sql->insert_id;
						$sql = $DB->prepare("UPDATE products SET img_preview_2=? WHERE id=?");
						$uploadfile_real = '/img-data/uploads/' .time().'_'. basename($_FILES['img_preview_2']['name']);
						$sql->bind_param('si', $uploadfile_real, $inserted_id);
						$sql->execute();
						$result = $sql->get_result();
					}
				}
				else
				{
					$error='Недопустимый формат изображения';
				}
			}
			else {
				$error='Изображение не загружено. Попробуйте ещё раз';
			}	
		}
		
		///////////превью фото 3
		if($_FILES["img_preview_3"]["size"]>0) 
		{
			$uploaddir = '../img-data/uploads/';
			if($_FILES["img_preview_3"]["error"] == 0)
			{
				if($_FILES['img_preview_3']['type'] == "image/gif" || $_FILES['img_preview_3']['type'] == "image/jpg" || $_FILES['img_preview_3']['type'] == "image/jpeg" || $_FILES['img_preview_3']['type'] == "image/png") 
				{
					$uploadfile = $uploaddir .time().'_'. basename($_FILES['img_preview_3']['name']);
					if(move_uploaded_file($_FILES['img_preview_3']['tmp_name'], $uploadfile)) {
						$inserted_id = $sql->insert_id;
						$sql = $DB->prepare("UPDATE products SET img_preview_3=? WHERE id=?");
						$uploadfile_real = '/img-data/uploads/' .time().'_'. basename($_FILES['img_preview_3']['name']);
						$sql->bind_param('si', $uploadfile_real, $inserted_id);
						$sql->execute();
						$result = $sql->get_result();
					}
				}
				else
				{
					$error='Недопустимый формат изображения';
				}
			}
			else {
				$error='Изображение не загружено. Попробуйте ещё раз';
			}	
		}
		
		///////////превью фото 4
		if($_FILES["img_preview_4"]["size"]>0) 
		{
			$uploaddir = '../img-data/uploads/';
			if($_FILES["img_preview_4"]["error"] == 0)
			{
				if($_FILES['img_preview_4']['type'] == "image/gif" || $_FILES['img_preview_4']['type'] == "image/jpg" || $_FILES['img_preview_4']['type'] == "image/jpeg" || $_FILES['img_preview_4']['type'] == "image/png") 
				{
					$uploadfile = $uploaddir .time().'_'. basename($_FILES['img_preview_4']['name']);
					if(move_uploaded_file($_FILES['img_preview_4']['tmp_name'], $uploadfile)) {
						$inserted_id = $sql->insert_id;
						$sql = $DB->prepare("UPDATE products SET img_preview_4=? WHERE id=?");
						$uploadfile_real = '/img-data/uploads/' .time().'_'. basename($_FILES['img_preview_4']['name']);
						$sql->bind_param('si', $uploadfile_real, $inserted_id);
						$sql->execute();
						$result = $sql->get_result();
					}
				}
				else
				{
					$error='Недопустимый формат изображения';
				}
			}
			else {
				$error='Изображение не загружено. Попробуйте ещё раз';
			}	
		}
		
        header("Location:/admin/?module=products");
    }
        
    static function edit_form($id)
    {
        global $DB;
        $temp = "";
        $sql = $DB->prepare("SELECT * FROM products WHERE id=?");
        $sql->bind_param('s', $id);
        $sql->execute();
        $result = $sql->get_result();
        while ($row = $result->fetch_assoc()) 
        {
            $id = $row['id'];
            
            $ru_title = $row['ru_title'];
            $ru_name = $row['ru_name'];
			$ru_about = $row['ru_about'];
			$ru_mini_description = $row['ru_mini_description'];
            $ru_keywords = $row['ru_keywords'];
            $ru_description = $row['ru_description'];
            $ru_content = html_entity_decode($row['ru_content']);
            
            $en_title = $row['en_title'];
            $en_name = $row['en_name'];
			$en_about = $row['en_about'];
			$en_mini_description = $row['en_mini_description'];
            $en_keywords = $row['en_keywords'];
            $en_description = $row['en_description'];
            $en_content = html_entity_decode($row['en_content']);
            
            $status = $row['status'];
            $price = $row['price'];
            $section_id = $row['section_id'];
        }
        
        $option = "<select name='section_id'>
        <option disabled>Выберите раздел</option>
        <option value='0'>Раздел не задан</option>";
        $sql_option = $DB->prepare("SELECT id, ru_name as name FROM sections");
        $sql_option->execute();
        $result_option = $sql_option->get_result();
        while ($row = $result_option->fetch_assoc()) 
        {
            if($row['id'] == $section_id)
                $option .=  '<option value="'.$row['id'].'" selected>'.$row['name'].'</option>';
            else
                $option .=  '<option value="'.$row['id'].'">'.$row['name'].'</option>';
        }
        $option .= "</select>";
        
        
        $temp = "<div class='forma'>
        <form action='/admin/?module=products&action=edit' method='POST' enctype='multipart/form-data'>
          <button type='button' class='btn btn-danger'><a href='/admin/?module=products&action=delete&id=$id' onClick= 'return window.confirm(`Вы уверены, что хотите удалить это изделие?`);'><span>Удалить изделие</span></a></button>
        <h3>Редактирование товара</h3><br />
		<br><h4><b>Страница на руссском языке</b></h4>
        <input type='hidden' name='id' value='$id'>
		<div class='col-md-6 col-xs-12'>
	   <p>Название товара: 
			<input type='text' name='ru_name' value='$ru_name' class='form-control' aria-describedby='sizing-addon2'>
		</p>
		<p>Описание о применении: 
			<input type='text' name='ru_about' value='$ru_about' class='form-control' aria-describedby='sizing-addon2'>
		</p>
		<p>Мини-описание товара: 
			<input type='text' name='ru_mini_description' value='$ru_mini_description' class='form-control' aria-describedby='sizing-addon2'>
		</p> 
		</div>
		<div class='col-md-6 col-xs-12'>
        <div class='block'>
            <p><b>TITLE: </b>
				<input type='text' name='ru_title' value='$ru_title' class='form-control' aria-describedby='sizing-addon2'>
			</p>
            <p><b>KEYWORDS: </b>
				<input type='text' name='ru_keywords' value='$ru_keywords' class='form-control' aria-describedby='sizing-addon2'>
			</p>
            <p><b>DESCRIPTION: </b>
				<input type='text' name='ru_description' value='$ru_description' class='form-control' aria-describedby='sizing-addon2'>
			</p></div></div>
			<p>Картинка продукции : 
				<input type='file' name='image' class='form-control' aria-describedby='sizing-addon2' accept='image/*,image/jpeg'>
			</p>
			<p>Первая миниатюра картинки: 
				<input type='file' name='img_preview_1' class='form-control' aria-describedby='sizing-addon2' accept='image/*,image/jpeg'>
			</p>
			<p>Вторая миниатюра картинки: 
				<input type='file' name='img_preview_2' class='form-control' aria-describedby='sizing-addon2' accept='image/*,image/jpeg'>
			</p>
			<p>Третья миниатюра картинки: 
				<input type='file' name='img_preview_3' class='form-control' aria-describedby='sizing-addon2' accept='image/*,image/jpeg'>
			</p>
			<p>Четвертая миниатюра картинки: 
				<input type='file' name='img_preview_4' class='form-control' aria-describedby='sizing-addon2' accept='image/*,image/jpeg'>
			</p>
        
        <p>Текст: <textarea id='ru_content_editor' name='ru_content' cols='100' rows='20'>$ru_content</textarea>
        <script type='text/javascript'>
            var ckeditor1 = CKEDITOR.replace( 'ru_content_editor' );
            AjexFileManager.init({
                returnTo: 'ckeditor',
                editor: ckeditor1
            });
        </script></p><br />
        
        <br><h4><b>Страница на английском языке</b></h4>
        <div class='col-md-6 col-xs-12'>
		<p>Name of product: 
			<input type='text' name='en_name' value='$en_name' class='form-control' aria-describedby='sizing-addon2'>
		</p>
		<p>Description of application: 
			<input type='text' name='en_about' value='$en_about' class='form-control' aria-describedby='sizing-addon2'>
		</p>
		<p>Mini-description: 
			<input type='text' name='en_mini_description' value='$en_mini_description' class='form-control' aria-describedby='sizing-addon2'>
		</p>
		</div>
		<div class='col-md-6 col-xs-12'>
        <div class='block'>
            <p><b>Title: </b>
				<input type='text' name='en_title' value='$en_title' class='form-control' aria-describedby='sizing-addon2'>
			</p>
            <p><b>Keywords: </b>
				<input type='text' name='en_keywords' value='$en_keywords' class='form-control' aria-describedby='sizing-addon2'>
			</p>
            <p><b>Description: </b>
				<input type='text' name='en_description' value='$en_description' class='form-control' aria-describedby='sizing-addon2'>
			</p>
        </div></div>
        <p>Text: <textarea id='en_content_editor' name='en_content' cols='100' rows='20'>$en_content</textarea>
        <script type='text/javascript'>
            var ckeditor1 = CKEDITOR.replace( 'en_content_editor' );
            AjexFileManager.init({
                returnTo: 'ckeditor',
                editor: ckeditor1
            });
        </script></p>
        <p>Цена(в рублях): <input type='text' name='price' value='$price'></p>";
        if($status =='1')
        {
            $temp .= "<p>Опубликовано: <input type='checkbox' name='status' value='1' checked></p>&nbsp;";
        }
        else
        {
            $temp .= "<p>Опубликовано: <input type='checkbox' name='status' value='1'></p>&nbsp;";
        }
        $temp .= $option;
        $temp .= "<p><br />
		 <button class='btn btn-secondary' type='submit' value='Изменить' name='submit' onClick= 'return window.confirm(`Изменить изделие?`);'>Изменить</button>
        </form>
        </div>";
        return $temp;
    }
    
    static function edit_submit()
    { 
        $id = $_POST['id'];
        
        $ru_title = $_POST['ru_title'];
        $ru_name = $_POST['ru_name'];
		$ru_about = $_POST['ru_about'];
		$ru_mini_description = $_POST['ru_mini_description'];
        $ru_keywords = $_POST['ru_keywords'];
        $ru_description = $_POST['ru_description'];
        $ru_content = html_entity_decode($_POST['ru_content']);
        
        $en_title = $_POST['en_title'];
        $en_name = $_POST['en_name'];
		$en_about = $_POST['en_about'];
		$en_mini_description = $_POST['en_mini_description'];
        $en_keywords = $_POST['en_keywords'];
        $en_description = $_POST['en_description'];
        $en_content = html_entity_decode($_POST['en_content']);
        
        $status = $_POST['status'];
        $price = $_POST['price'];
        $section_id = $_POST['section_id'];
       
        global $DB;
        $sql = $DB->prepare("UPDATE products SET ru_title=?, ru_name=?, ru_about=?, ru_mini_description=?, ru_keywords=?, ru_description=?, ru_content=?, en_title=?, en_name=?, en_about=?, en_mini_description=?, en_keywords=?, en_description=?, en_content=?, status=?, price=?, section_id=? WHERE id=?");
        $sql->bind_param('ssssssssssssssssss', $ru_title, $ru_name, $ru_about, $ru_mini_description, $ru_keywords, $ru_description, $ru_content, $en_title, $en_name, $en_about, $en_mini_description, $en_keywords, $en_description, $en_content, $status, $price, $section_id, $id);
        $sql->execute();
        $result = $sql->get_result();
		
		
		//fdddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddd
		if($_FILES["image"]["size"]>0) 
		{
			$uploaddir = '../img-data/uploads/';
			if($_FILES["image"]["error"] == 0)
			{
				if($_FILES['image']['type'] == "image/gif" || $_FILES['image']['type'] == "image/jpg" || $_FILES['image']['type'] == "image/jpeg" || $_FILES['image']['type'] == "image/png") 
				{
					$uploadfile = $uploaddir .time().'_'. basename($_FILES['image']['name']);
					if(move_uploaded_file($_FILES['image']['tmp_name'], $uploadfile)) {
						$sql = $DB->prepare("UPDATE products SET image=? WHERE id=?");
						
						$uploadfile_real = '/img-data/uploads/' .time().'_'. basename($_FILES['image']['name']);
						
						$sql->bind_param('si', $uploadfile_real, $id);
						$sql->execute();
						$result = $sql->get_result();
					}
				}
				else
				{
					$error='Недопустимый формат изображения';
				}
			}
			else {
				$error='Изображение не загружено. Попробуйте ещё раз';
			}	
		}
		
		/////превью картинки 1
		if($_FILES["img_preview_1"]["size"]>0) 
		{
			$uploaddir = '../img-data/uploads/';
			if($_FILES["img_preview_1"]["error"] == 0)
			{
				if($_FILES['img_preview_1']['type'] == "image/gif" || $_FILES['img_preview_1']['type'] == "image/jpg" || $_FILES['img_preview_1']['type'] == "image/jpeg" || $_FILES['img_preview_1']['type'] == "image/png") 
				{
					$uploadfile = $uploaddir .time().'_'. basename($_FILES['img_preview_1']['name']);
					if(move_uploaded_file($_FILES['img_preview_1']['tmp_name'], $uploadfile)) {
						$sql = $DB->prepare("UPDATE products SET img_preview_1=? WHERE id=?");
						
						$uploadfile_real = '/img-data/uploads/' .time().'_'. basename($_FILES['img_preview_1']['name']);
						
						$sql->bind_param('si', $uploadfile_real, $id);
						$sql->execute();
						$result = $sql->get_result();
					}
				}
				else
				{
					$error='Недопустимый формат изображения';
				}
			}
			else {
				$error='Изображение не загружено. Попробуйте ещё раз';
			}	
		}
		
		
			/////превью картинки 2
		if($_FILES["img_preview_2"]["size"]>0) 
		{
			$uploaddir = '../img-data/uploads/';
			if($_FILES["img_preview_2"]["error"] == 0)
			{
				if($_FILES['img_preview_2']['type'] == "image/gif" || $_FILES['img_preview_2']['type'] == "image/jpg" || $_FILES['img_preview_2']['type'] == "image/jpeg" || $_FILES['img_preview_2']['type'] == "image/png") 
				{
					$uploadfile = $uploaddir .time().'_'. basename($_FILES['img_preview_2']['name']);
					if(move_uploaded_file($_FILES['img_preview_2']['tmp_name'], $uploadfile)) {
						$sql = $DB->prepare("UPDATE products SET img_preview_2=? WHERE id=?");
						
						$uploadfile_real = '/img-data/uploads/' .time().'_'. basename($_FILES['img_preview_2']['name']);
						
						$sql->bind_param('si', $uploadfile_real, $id);
						$sql->execute();
						$result = $sql->get_result();
					}
				}
				else
				{
					$error='Недопустимый формат изображения';
				}
			}
			else {
				$error='Изображение не загружено. Попробуйте ещё раз';
			}	
		}
		
			/////превью картинки 3
		if($_FILES["img_preview_3"]["size"]>0) 
		{
			$uploaddir = '../img-data/uploads/';
			if($_FILES["img_preview_3"]["error"] == 0)
			{
				if($_FILES['img_preview_3']['type'] == "image/gif" || $_FILES['img_preview_3']['type'] == "image/jpg" || $_FILES['img_preview_3']['type'] == "image/jpeg" || $_FILES['img_preview_3']['type'] == "image/png") 
				{
					$uploadfile = $uploaddir .time().'_'. basename($_FILES['img_preview_3']['name']);
					if(move_uploaded_file($_FILES['img_preview_3']['tmp_name'], $uploadfile)) {
						$sql = $DB->prepare("UPDATE products SET img_preview_3=? WHERE id=?");
						
						$uploadfile_real = '/img-data/uploads/' .time().'_'. basename($_FILES['img_preview_3']['name']);
						
						$sql->bind_param('si', $uploadfile_real, $id);
						$sql->execute();
						$result = $sql->get_result();
					}
				}
				else
				{
					$error='Недопустимый формат изображения';
				}
			}
			else {
				$error='Изображение не загружено. Попробуйте ещё раз';
			}	
		}
		
			/////превью картинки 4
		if($_FILES["img_preview_4"]["size"]>0) 
		{
			$uploaddir = '../img-data/uploads/';
			if($_FILES["img_preview_4"]["error"] == 0)
			{
				if($_FILES['img_preview_4']['type'] == "image/gif" || $_FILES['img_preview_4']['type'] == "image/jpg" || $_FILES['img_preview_4']['type'] == "image/jpeg" || $_FILES['img_preview_4']['type'] == "image/png") 
				{
					$uploadfile = $uploaddir .time().'_'. basename($_FILES['img_preview_4']['name']);
					if(move_uploaded_file($_FILES['img_preview_4']['tmp_name'], $uploadfile)) {
						$sql = $DB->prepare("UPDATE products SET img_preview_4=? WHERE id=?");
						
						$uploadfile_real = '/img-data/uploads/' .time().'_'. basename($_FILES['img_preview_4']['name']);
						
						$sql->bind_param('si', $uploadfile_real, $id);
						$sql->execute();
						$result = $sql->get_result();
					}
				}
				else
				{
					$error='Недопустимый формат изображения';
				}
			}
			else {
				$error='Изображение не загружено. Попробуйте ещё раз';
			}	
		}
		
        header("Location:/admin/?module=products");
    }
    
    static function delete($id)
    {
        global $DB;
        $sql = $DB->prepare("DELETE FROM products WHERE id=?");
        $sql->bind_param('s', $id);
        $sql->execute();
        header("Location:/admin/?module=products");
    }
}

#####################################################################
//Модуль партнеров предприятия
#####################################################################
class admin_partners
{
    static function get_list()
    {
        global $DB;
        
        $temp = "<button type='button' class='btn btn-success'><i class='fa fa-plus-circle' aria-hidden='true' style='margin-right:15px;'></i><a href='/admin/?module=partners&action=add'>Добавить партнеров</a></button>
        <table class='table table-bordered'>
        <thead class='thead-dark'>
        <tr>
        <th>id партнера</th>
        <th>Название</th>
		<th>Общее описание</th>
		<th>Страна</th>
		<th>Сколько лет на рынке</th>
        <th>Статус публикации на странице</th>
        </tr>
        </thead>";
        $sql = $DB->prepare("SELECT id, ru_name as name, ru_content as content, ru_country as country, ru_work_years as work_years, ru_direction as direction, status FROM partners");
        $sql->execute();
        $result = $sql->get_result();
        while ($row = $result->fetch_assoc()) 
        {
			
			$class = '';
			if($row['status'] == 1)
			{
				$class="admin_published";
			}
			else{
				$class="admin_published1";
			}
		
				
			
            if($row['status'] == 1)
            {
                $row['status'] = 'Опубликован';
            }
            else
            {
                $row['status'] = 'Не опубликован';
            }
            $temp .= "<tr class='".$class."' onclick='window.location.href=`/admin/?module=partners&action=edit&id=$row[id]`; return false'>
                        <td>$row[id]</td>
                        <td>$row[name]</td>
						<td>$row[content]</td>
						<td>$row[country]</td>
						<td>$row[work_years]</td>
                        <td>$row[status]</td>
                    </tr>";
        }
        $sql->close();
        $temp .= "</table>";
        return $temp;
    }
	
	static function add_form()
    {
        $temp = "<div class='forma'>
        <form action='/admin/?module=partners&action=add' method='POST' enctype='multipart/form-data'>
        <h3>Добавление партнеров предприятия</h3><br />
		<h4><b>Страница на русском языке</b></h4>
		<div class='col-md-6 col-xs-12'>
	   <p>Наименование организации: 
			<input type='text' placeholder='Введите наименование...' name='ru_name' class='form-control' aria-describedby='sizing-addon2'>
		</p> 
		<p>Страна: 
			<input type='text' name='ru_country' placeholder='Введите страну' class='form-control' aria-describedby='sizing-addon2'>
		</p>
		<p>Сколько лет на рынке: 
			<input type='text'  placeholder='Сколько лет на рынке...' name='ru_work_years' class='form-control' aria-describedby='sizing-addon2'>
		</p>
		</div>
		<div class='col-md-6 col-xs-12'>
        <div class='block'>
            <p><b>TITLE: </b>
				<input type='text' name='ru_title' placeholder='Введите заголовок...' class='form-control' aria-describedby='sizing-addon2'>
			</p>
            <p><b>KEYWORDS: </b>
				<input type='text' name='ru_keywords' placeholder='Введите ключевые слова...' class='form-control' aria-describedby='sizing-addon2'>
			</p>
            <p><b>DESCRIPTION: </b>
				<input type='text' name='ru_description' placeholder='Введите описане...' class='form-control' aria-describedby='sizing-addon2'>
			</p>
        </div></div>
        <p>Общее описание организации: <textarea id='ru_content_editor' name='ru_content' cols='100' rows='20'></textarea>
        <script type='text/javascript'>
            var ckeditor1 = CKEDITOR.replace( 'ru_content_editor' );
            AjexFileManager.init({
                returnTo: 'ckeditor',
                editor: ckeditor1
            });
        </script></p>
		<p>По каким направлениям работают: 
			<input type='text' name='ru_direction' placeholder='Введите направление' class='form-control' aria-describedby='sizing-addon2'>
		</p>
		<p>Логотип партнеров: 
				<input type='file' name='image' class='form-control' aria-describedby='sizing-addon2' accept='image/*,image/jpeg'>
		</p>
		
 		<br />
        
        <br><h4><b>Страница на английском языке</b></h4>
        <div class='col-md-6 col-xs-12'>
		<p>Name of organization: 
			<input type='text' placeholder='Write name of organization...' name='en_name' class='form-control' aria-describedby='sizing-addon2'>
		</p> 
		<p>Country: 
			<input type='text' placeholder='Write country...' name='en_country' class='form-control' aria-describedby='sizing-addon2'>
		</p>
		<p>How many years in the market: 
			<input type='text'  placeholder='Write work years...' name='en_work_years' class='form-control' aria-describedby='sizing-addon2'>
		</p>
		</div>
		<div class='col-md-6 col-xs-12'>
        <div class='block'>
            <p><b>Title: </b>
				<input type='text' placeholder='Write title...' name='en_title' class='form-control' aria-describedby='sizing-addon2'>
			</p>
            <p><b>Keywords: </b>
				<input type='text' placeholder='Write keywords...' name='en_keywords' class='form-control' aria-describedby='sizing-addon2'>
			</p>
            <p><b>Description: </b>
				<input type='text' placeholder='Write description...' name='en_description' class='form-control' aria-describedby='sizing-addon2'>
			</p>
        </div>
		</div>
        <p>General description of organization: <textarea id='en_content_editor' name='en_content' cols='100' rows='20'></textarea>
        <script type='text/javascript'>
            var ckeditor1 = CKEDITOR.replace( 'en_content_editor' );
            AjexFileManager.init({
                returnTo: 'ckeditor',
                editor: ckeditor1
            });
        </script></p>
		<p>Directions: 
			<input type='text' placeholder='Write directions' name='en_direction' class='form-control' aria-describedby='sizing-addon2'>
		</p>
        
		<p>Опубликовать: <input type='checkbox' name='status' value='1'></p>
        <button class='btn btn-secondary' type='submit' value='Добавить' name='submit' onClick= 'return window.confirm(`Добавить новый раздел??`);'>Добавить</button>
        </form>
        </div>";
		return $temp;
    }
	
	 static function add_submit()
    {
        $ru_name = $_POST['ru_name'];
		$ru_country = $_POST['ru_country'];
		$ru_work_years = $_POST['ru_work_years'];
        $ru_title = $_POST['ru_title'];
        $ru_keywords = $_POST['ru_keywords'];
        $ru_description = $_POST['ru_description'];
		$ru_content = $_POST['ru_content'];
		$ru_direction = $_POST['ru_direction'];
		
        
        $en_name = $_POST['en_name'];
		$en_country = $_POST['en_country'];
		$en_work_years = $_POST['en_work_years'];
        $en_title = $_POST['en_title'];
        $en_keywords = $_POST['en_keywords'];
        $en_description = $_POST['en_description'];
		$en_content = $_POST['en_content'];
        $en_direction = $_POST['en_direction'];
		
        
        $ru_url = CMS::translit($ru_name);
        $en_url = CMS::translit($en_name);
		
		$status = $_POST['status'];
        
        global $DB;
        $sql = $DB->prepare("INSERT INTO partners (ru_name, ru_country, ru_work_years, ru_title, ru_keywords, ru_description, ru_content, ru_direction, en_name,en_country, en_work_years, en_title, en_keywords, en_description, en_content, en_direction, ru_url, en_url,status) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
        if(!$sql) echo($DB->error);
		$sql->bind_param('ssssssssssssssssss', $ru_name, $ru_country, $ru_work_years, $ru_title, $ru_keywords, $ru_description, $ru_content, $ru_direction, $en_name, $en_country, $en_work_years, $en_title, $en_keywords, $en_description, $en_content,  $en_direction, $ru_url, $en_url, $status);
        if(!$sql) echo($DB->error);
		$sql->execute();
        $result = $sql->get_result();
		if($_FILES["image"]["size"]>0) 
		{
			$uploaddir = '../img-data/uploads/';
			if($_FILES["image"]["error"] == 0)
			{
				if($_FILES['image']['type'] == "image/gif" || $_FILES['image']['type'] == "image/jpg" || $_FILES['image']['type'] == "image/jpeg" || $_FILES['image']['type'] == "image/png") 
				{
					$uploadfile = $uploaddir .time().'_'. basename($_FILES['image']['name']);
					if(move_uploaded_file($_FILES['image']['tmp_name'], $uploadfile)) {
						$inserted_id = $sql->insert_id;
						$sql = $DB->prepare("UPDATE partners SET image=? WHERE id=?");
						$uploadfile_real = '/img-data/uploads/' .time().'_'. basename($_FILES['image']['name']);
						$sql->bind_param('si', $uploadfile_real, $inserted_id);
						$sql->execute();
						$result = $sql->get_result();
					}
				}
				else
				{
					$error='Недопустимый формат изображения';
				}
			}
			else {
				$error='Изображение не загружено. Попробуйте ещё раз';
			}	
		}
		
		
        header("Location:/admin/?module=partners");
    }
	
	
	static function edit_form($id)
    {
        global $DB;
        $temp = "";
        $sql = $DB->prepare("SELECT * FROM partners WHERE id=?");
        $sql->bind_param('s', $id);
        $sql->execute();
        $result = $sql->get_result();
        while ($row = $result->fetch_assoc()) 
        {
            $id = $row['id'];
            
			$ru_name = $row['ru_name'];
			$ru_country = $row['ru_country'];
			$ru_work_years = $row['ru_work_years'];
            $ru_title = $row['ru_title'];
            $ru_keywords = $row['ru_keywords'];
            $ru_description = $row['ru_description'];
            $ru_content = html_entity_decode($row['ru_content']);
            $ru_direction = $row['ru_direction'];
            
            
            $en_name = $row['en_name'];
			$en_country = $row['en_country'];
            $en_work_years = $row['en_work_years'];
			$en_title = $row['en_title'];
            $en_keywords = $row['en_keywords'];
            $en_description = $row['en_description'];
            $en_content = html_entity_decode($row['en_content']);
			$en_direction = $row['en_direction'];
			
			
            $status = $row['status'];
          
        }
        $temp = "<div class='forma'>
        <form action='/admin/?module=partners&action=edit' method='POST' enctype='multipart/form-data'>
        <input type='hidden' name='id' value='$id'>
		  <button type='button' class='btn btn-danger'><a href='/admin/?module=partners&action=delete&id=$id' onClick= 'return window.confirm(`Вы уверены, что хотите удалить страницу?`);'><span>Удалить страницу</span></a></button>
		<h3>Редактирование партнеров предприятия</h3><br />
		<br><h4><b>Страница на руссском языке</b></h4>
		<div class='col-md-6 col-xs-12'>
		<p>Наименование организации: 
			<input type='text' name='ru_name' value='$ru_name' class='form-control' aria-describedby='sizing-addon2'>
		</p> 
		<p>Страна: 
			<input type='text' name='ru_country'  value='$ru_country' class='form-control' aria-describedby='sizing-addon2'>
		</p>
		<p>Сколько лет на рынке: 
			<input type='text' name='ru_work_years'  value='$ru_work_years' class='form-control' aria-describedby='sizing-addon2'>
		</p>
		</div>
		<div class='col-md-6 col-xs-12'>
        <div class='block'>
            <p><b>TITLE: </b>
				<input type='text' name='ru_title'  value='$ru_title' class='form-control' aria-describedby='sizing-addon2'>
			</p>
            <p><b>KEYWORDS: </b>
				<input type='text' name='ru_keywords'  value='$ru_keywords' class='form-control' aria-describedby='sizing-addon2'>
			</p>
            <p><b>DESCRIPTION: </b>
				<input type='text' name='ru_description'  value='$ru_description' class='form-control' aria-describedby='sizing-addon2'>
			</p>
        </div></div>
        <p>Общее описание организации: <textarea id='ru_content_editor' name='ru_content' cols='100' rows='20'>$ru_content</textarea>
        <script type='text/javascript'>
            var ckeditor1 = CKEDITOR.replace( 'ru_content_editor' );
            AjexFileManager.init({
                returnTo: 'ckeditor',
                editor: ckeditor1
            });
        </script></p>
		<p>По каким направлениям работают: 
			<input type='text' name='ru_direction'  value='$ru_direction' class='form-control' aria-describedby='sizing-addon2'>
		</p>
		<p>Картинка продукции: 
				<input type='file' name='image' class='form-control' aria-describedby='sizing-addon2' accept='image/*,image/jpeg'>
		</p>
 		<br /><br />
        
         <br><h4><b>Страница на английском языке</b></h4>
        <div class='col-md-6 col-xs-12'>
		<p>Name of organization: 
			<input type='text' name='en_name' value='$en_name' class='form-control' aria-describedby='sizing-addon2'>
		</p>
		<p>Country: 
			<input type='text' name='en_country'  value='$en_country' class='form-control' aria-describedby='sizing-addon2'>
		</p>
		<p>How many years in the market: 
			<input type='text' name='en_work_years'  value='$en_work_years' class='form-control' aria-describedby='sizing-addon2'>
		</p>
		</div>
		<div class='col-md-6 col-xs-12'>
        <div class='block'>
            <p><b>Title: </b>
				<input type='text' name='en_title' value='$en_title' class='form-control' aria-describedby='sizing-addon2'>
			</p>
            <p><b>Keywords: </b>
				<input type='text' name='en_keywords'  value='$en_keywords' class='form-control' aria-describedby='sizing-addon2'>
			</p>
            <p><b>Description: </b>
				<input type='text' name='en_description' p value='$en_description' class='form-control' aria-describedby='sizing-addon2'>
			</p>
        </div>
		</div>
        <p>General information about organization: <textarea id='en_content_editor' name='en_content' cols='100' rows='20'>$en_content</textarea>
        <script type='text/javascript'>
            var ckeditor1 = CKEDITOR.replace( 'en_content_editor' );
            AjexFileManager.init({
                returnTo: 'ckeditor',
                editor: ckeditor1
            });
        </script></p>
		&nbsp;";
       if($status =='1')
        {
            $temp .= "<p>Опубликовать: <input type='checkbox' name='status' value='1' checked></p>";
        }
        else
        {
            $temp .= "<p>Опубликовано: <input type='checkbox' name='status' value='1'></p>";
        }
        $temp .= "
		<button class='btn btn-secondary' type='submit' value='Изменить' name='submit' onClick= 'return window.confirm(`Вы уверены, что хотите применить изменения к этой странице?`);'>Изменить</button>
        </form>
        </div>";
        return $temp;
    }
	
	static function edit_submit()
    { 
        $id = $_POST['id'];
            
            $ru_name = $_POST['ru_name'];
			$ru_country = $_POST['ru_country'];
			$ru_work_years = $_POST['ru_work_years'];
			$ru_title = $_POST['ru_title'];
			$ru_keywords = $_POST['ru_keywords'];
			$ru_description = $_POST['ru_description'];
			$ru_content = html_entity_decode($_POST['ru_content']);
			
        
			$en_name = $_POST['en_name'];
			$en_country = $_POST['en_country'];
			$en_work_years = $_POST['en_work_years'];
			$en_title = $_POST['en_title'];
			$en_keywords = $_POST['en_keywords'];
			$en_description = $_POST['en_description'];
			$en_content = html_entity_decode($_POST['en_content']);
			
			  
            $status = $_POST['status'];
			
			$ru_url = CMS::translit($ru_name);
            $en_url = CMS::translit($en_name);
            
            
        global $DB;
        $sql = $DB->prepare("UPDATE partners SET ru_name=?, ru_country=?, ru_work_years=?, ru_title=?, ru_keywords=?, ru_description=?, ru_content=?, en_name=?,en_country=?, en_work_years=?, en_title=?, en_keywords=?, en_description=?, en_content=?, ru_url=?, en_url=?, status=? WHERE id=?");
        $sql->bind_param('ssssssssssssssssss', $ru_name, $ru_country, $ru_work_years, $ru_title, $ru_keywords, $ru_description, $ru_content, $en_name,$en_country, $en_work_years, $en_title, $en_keywords, $en_description, $en_content, $ru_url, $en_url, $status, $id);
        $sql->execute();
        $result = $sql->get_result();
		
		if($_FILES["image"]["size"]>0) 
		{
			$uploaddir = '../img-data/uploads/';
			if($_FILES["image"]["error"] == 0)
			{
				if($_FILES['image']['type'] == "image/gif" || $_FILES['image']['type'] == "image/jpg" || $_FILES['image']['type'] == "image/jpeg" || $_FILES['image']['type'] == "image/png") 
				{
					$uploadfile = $uploaddir .time().'_'. basename($_FILES['image']['name']);
					if(move_uploaded_file($_FILES['image']['tmp_name'], $uploadfile)) {
						$sql = $DB->prepare("UPDATE partners SET image=? WHERE id=?");
						
						$uploadfile_real = '/img-data/uploads/' .time().'_'. basename($_FILES['image']['name']);
						
						$sql->bind_param('si', $uploadfile_real, $id);
						$sql->execute();
						$result = $sql->get_result();
					}
				}
				else
				{
					$error='Недопустимый формат изображения';
				}
			}
			else {
				$error='Изображение не загружено. Попробуйте ещё раз';
			}	
		}
		
		
      header("Location:/admin/?module=partners");
    }
	
	 static function delete($id)
    {
        global $DB;
        $sql = $DB->prepare("DELETE FROM partners WHERE id=?");
        $sql->bind_param('s', $id);
        $sql->execute();
        header("Location:/admin/?module=partners");
    }
	
}
#####################################################################
//Модуль имнформации о компании
#####################################################################
class admin_about_company{
	
	static function get_list()
    {
        global $DB;
        
        $temp = "<button type='button' class='btn btn-success'><i class='fa fa-plus-circle' aria-hidden='true' style='margin-right:15px;'></i><a href='/admin/?module=admin_about_company&action=add'>Добавить информацию</a></button>
        <table class='table table-bordered'>
		<thead class='thead-dark'>
        <tr>
		<th>Название секции</th>
        <th>Заголовок страницы</th>
		<th>Контент страницы</th>
        <th>Статус публикации на странице</th>
        </tr>
        </thead>";
        $sql = $DB->prepare("SELECT id, ru_name as name, ru_title as title, ru_content as content, status FROM about_company");
        $sql->execute();
        $result = $sql->get_result();
        while ($row = $result->fetch_assoc()) 
        {
			$class = '';
			if($row['status'] == 1)
			{
				$class="admin_published";
			}
			else{
				$class="admin_published1";
			}
			
            if($row['status'] == 1)
            {
                $row['status'] = 'Опубликована';
            }
            else
            {
                $row['status'] = 'Не опубликована';
            }
            $temp .= "<tr class='".$class."' onclick='window.location.href=`/admin/?module=admin_about_company&action=edit&id=$row[id]`; return false'>
                        <td>$row[name]</td>
						<td>$row[title]</td>
						<td>$row[content]</td>
                        <td>$row[status]</td>
                    </tr>";
        }
        $sql->close();
        $temp .= "</table>";
        return $temp;
    }
	
	
	static function add_form()
    {
        $temp = "<div class='forma'>
        <form action='/admin/?module=admin_about_company&action=add' method='POST'>
		<h3>Информация о предприятии</h3><br />
		<h4><b>Страница на русском языке</b></h4>
        <div class='col-md-6 col-xs-12'>
		<p>Наименование секции: 
			<input type='text' name='ru_name' placeholder='Введите название секции...' class='form-control' aria-describedby='sizing-addon2'>
		</p>
		</div>
		<div class='col-md-6 col-xs-12'>
		<div class='block'>
            <p><b>TITLE: </b>
				<input type='text' name='ru_title' placeholder='Введите заголовок...' class='form-control' aria-describedby='sizing-addon2'>
			</p>
            <p><b>KEYWORDS: </b>
				<input type='text' name='ru_keywords' placeholder='Введите ключевые слова...' class='form-control' aria-describedby='sizing-addon2'>
			</p>
            <p><b>DESCRIPTION: </b>
				<input type='text' name='ru_description' placeholder='Введите описане...' class='form-control' aria-describedby='sizing-addon2'>
			</p>
        </div></div>
        <p>Дополнительная информация: <textarea id='ru_content_editor' name='ru_content' cols='100' rows='20'></textarea>
        <script type='text/javascript'>
            var ckeditor1 = CKEDITOR.replace( 'ru_content_editor' );
            AjexFileManager.init({
                returnTo: 'ckeditor',
                editor: ckeditor1
            });
        </script></p>
 		<br />
        
        <br><h4><b>Страница на английском языке</b></h4>
        <div class='block'>
		<div class='col-md-6 col-xs-12'>
		<p>Name of section: 
			<input type='text' name='en_name' placeholder='Write name section...' class='form-control' aria-describedby='sizing-addon2'>
		</p>
		</div>
		<div class='col-md-6 col-xs-12'>
            <p><b>Title: </b>
				<input type='text' name='en_title' placeholder='Write a title...' class='form-control' aria-describedby='sizing-addon2'>
			</p>
            <p><b>Keywords: </b>
				<input type='text' name='en_keywords' placeholder='Write a keywords...' class='form-control' aria-describedby='sizing-addon2'>
			</p>
            <p><b>Description: </b>
				<input type='text' name='en_description'placeholder='Write a description...' class='form-control' aria-describedby='sizing-addon2'>
			</p>
        </div></div>
        <p>Additional information: <textarea id='en_content_editor' name='en_content' cols='100' rows='20'></textarea>
        <script type='text/javascript'>
            var ckeditor1 = CKEDITOR.replace( 'en_content_editor' );
            AjexFileManager.init({
                returnTo: 'ckeditor',
                editor: ckeditor1
            });
        </script></p>
        
		<p>Опубликовать: <input type='checkbox' name='status' value='1'></p>
        <button class='btn btn-secondary' type='submit' value='Добавить' name='submit' onClick= 'return window.confirm(`Добавить новый раздел??`);'>Добавить</button>
        </form>
        </div>";
		return $temp;
    }
	
	 static function add_submit()
    {
		
		$ru_name = $_POST['ru_name'];
        $ru_title = $_POST['ru_title'];
        $ru_keywords = $_POST['ru_keywords'];
        $ru_description = $_POST['ru_description'];
		$ru_content = html_entity_decode($_POST['ru_content']);
		
        $en_name = $_POST['en_name'];
        $en_title = $_POST['en_title'];
        $en_keywords = $_POST['en_keywords'];
        $en_description = $_POST['en_description'];
		$en_content = html_entity_decode($_POST['en_content']);
		
		$ru_url = CMS::translit($ru_name);
        $en_url = CMS::translit($en_name);
		
		$status = $_POST['status'];
        
        global $DB;
        $sql = $DB->prepare("INSERT INTO about_company (ru_name,ru_title, ru_keywords, ru_description, ru_content, en_name, en_title, en_keywords, en_description, en_content, status, ru_url, en_url) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)");
		$sql->bind_param('sssssssssssss',$ru_name, $ru_title, $ru_keywords, $ru_description, $ru_content,$en_name, $en_title, $en_keywords, $en_description, $en_content, $status, $ru_url, $en_url);
		$sql->execute();
        $result = $sql->get_result();
        header("Location:/admin/?module=admin_about_company");
    }
	
	 static function edit_form($id)
    {
        global $DB;
        $temp = "";
        $sql = $DB->prepare("SELECT * FROM about_company WHERE id=?");
        $sql->bind_param('s', $id);
        $sql->execute();
        $result = $sql->get_result();
        while ($row = $result->fetch_assoc()) 
        {
            $id = $row['id'];
            
            $ru_name = $row['ru_name'];
			$ru_title = $row['ru_title'];
            $ru_keywords = $row['ru_keywords'];
            $ru_description = $row['ru_description'];
			$ru_content = html_entity_decode($row['ru_content']);
            
            $en_name = $row['en_name'];
			$en_title = $row['en_title'];
            $en_keywords = $row['en_keywords'];
            $en_description = $row['en_description'];
			$en_content = html_entity_decode($row['en_content']);
            
            $status = $row['status'];
        }
        $temp = "<div class='forma'>
        <form action='/admin/?module=admin_about_company&action=edit' method='POST'>
        <input type='hidden' name='id' value='$id'>
          <button type='button' class='btn btn-danger'><a href='/admin/?module=admin_about_company&action=delete&id=$id' onClick= 'return window.confirm(`Вы уверены, что хотите удалить раздел?`);'><span>Удалить раздел</span></a></button>
        <h3>Редактирование страницы о предприятии</h3><br />
		<h4><b>Страница на русском языке</b></h4>
        <div class='col-md-6 col-xs-12'>
		<p>Название секции: 
			<input type='text' name='ru_name' value='$ru_name' class='form-control' aria-describedby='sizing-addon2'>
		</p> 
		</div>
		<div class='col-md-6 col-xs-12'>
        <div class='block'>
            <p><b>TITLE: </b>
				<input type='text' name='ru_title' value='$ru_title' class='form-control' aria-describedby='sizing-addon2'>
			</p>
            <p><b>KEYWORDS: </b>
				<input type='text' name='ru_keywords' value='$ru_keywords' class='form-control' aria-describedby='sizing-addon2'>
			</p>
            <p><b>DESCRIPTION: </b>
				<input type='text' name='ru_description' value='$ru_description' class='form-control' aria-describedby='sizing-addon2'>
			</p>
        </div></div>
		<p>Контент страницы: <textarea id='ru_content_editor' name='ru_content' cols='100' rows='20'>$ru_content</textarea>
        <script type='text/javascript'>
            var ckeditor1 = CKEDITOR.replace( 'ru_content_editor' );
            AjexFileManager.init({
                returnTo: 'ckeditor',
                editor: ckeditor1
            });
        </script></p><br />
        
        <h4><b>Страница на английском языке</b></h4>
        <div class='col-md-6 col-xs-12'>
		<p>Name of page: 
			<input type='text' name='en_name' value='$en_name' class='form-control' aria-describedby='sizing-addon2'>
		</p>
		</div>
		<div class='col-md-6 col-xs-12'>
        <div class='block'>
            <p><b>Title: </b>
				<input type='text' name='en_title' value='$en_title' class='form-control' aria-describedby='sizing-addon2'>
			</p>
            <p><b>Keywords:</b> 
				<input type='text' name='en_keywords' value='$en_keywords' class='form-control' aria-describedby='sizing-addon2'>
			</p>
            <p><b>Description: </b>
				<input type='text' name='en_description' value='$en_description' class='form-control' aria-describedby='sizing-addon2'>
			</p>
        </div></div>
		<p>Cpntent page: <textarea id='en_content_editor' name='en_content' cols='100' rows='20'>$en_content</textarea>
        <script type='text/javascript'>
            var ckeditor1 = CKEDITOR.replace( 'en_content_editor' );
            AjexFileManager.init({
                returnTo: 'ckeditor',
                editor: ckeditor1
            });
        </script></p><br />&nbsp";
        if($status =='1')
        {
            $temp .= "<p>Опубликован: <input type='checkbox' name='status' value='1' checked></p>";
        }
        else
        {
            $temp .= "<p>Опубликован: <input type='checkbox' name='status' value='1'></p>";
        }
        $temp .= "<button class='btn btn-secondary' type='submit' value='Изменить' name='submit' onClick= 'return window.confirm(`Изменить раздел??`);'>Изменить</button>
        </form>
        </div>";
        return $temp;
    }
    
    static function edit_submit()
    { 
        $id = $_POST['id'];
            
            $ru_name = $_POST['ru_name'];
			$ru_title = $_POST['ru_title'];
            $ru_keywords = $_POST['ru_keywords'];
            $ru_description = $_POST['ru_description'];
			$ru_content = html_entity_decode($_POST['ru_content']);
            
            $en_name = $_POST['en_name'];
			$en_title = $_POST['en_title'];
            $en_keywords = $_POST['en_keywords'];
            $en_description = $_POST['en_description'];
			$en_content = html_entity_decode($_POST['en_content']);
              
            $status = $_POST['status'];
            
            $ru_url = CMS::translit($ru_name);
            $en_url = CMS::translit($en_name);
       
        global $DB;
        $sql = $DB->prepare("UPDATE about_company SET  ru_name=?, ru_title=?, ru_keywords=?, ru_description=?, ru_content=?, en_name=?, en_title=?,  en_keywords=?, en_description=?, en_content=?, status=?, ru_url=?, en_url=? WHERE id=?");
        $sql->bind_param('ssssssssssssss', $ru_name, $ru_title,  $ru_keywords, $ru_description, $ru_content,  $en_title, $en_name, $en_keywords, $en_description, $en_content, $status, $ru_url, $en_url, $id);
        $sql->execute();
		//var_dump($DB);
        $result = $sql->get_result();
        header("Location:/admin/?module=admin_about_company");
    }
    
    function delete($id)
    {
        global $DB;
        $sql = $DB->prepare("DELETE FROM about_company WHERE id=?");
        $sql->bind_param('s', $id);
        $sql->execute();
        header("Location:/admin/?module=admin_about_company");
    }
}


#####################################################################
//Модуль информации истории компании
#####################################################################
class admin_history{
	
	static function get_list()
    {
        global $DB;
        $temp = "<button type='button' class='btn btn-success'><i class='fa fa-plus-circle' aria-hidden='true' style='margin-right:15px;'></i><a href='/admin/?module=history&action=add'>Добавить страницу</a></button>
		<table class='table table-hover'>
        <thead class='thead-dark'>
        <tr>
        <th>Название блока</th>
		<th>Заголовок раздела</th>
        <th>Основной контент страницы</th>
        <th>Статус публикации на странице</th>
        </tr>
        </thead>";
        $sql = $DB->prepare("SELECT id, ru_name as name, ru_title as title, ru_content as content, status FROM history_company");
        $sql->execute();
        $result = $sql->get_result();
        while ($row = $result->fetch_assoc()) 
        {
            if($row['status'] == 1)
            {
                $row['status'] = 'Опубликована';
            }
            else
            {
                $row['status'] = 'Не опубликована';
            }
            $temp .= "<tr onclick='window.location.href=`/admin/?module=history&action=edit&id=$row[id]`; return false'>
						<td>$row[name]</td>
						<td>$row[title]</td>
						<td>$row[content]</td>
                        <td>$row[status]</td>
					</tr>";
        }
        $sql->close();
        $temp .= "</table>";
        return $temp;
		return "";
    }
    
    static function add_form()
    {
        return "<div class='forma'>
        <form action='/admin/?module=history&action=add' method='POST'>
        <h3>Добавление новой страницы</h3><br />
		<h4><b>Страница на русском языке</b></h4><br />
		<div class='col-md-6 col-xs-12'>
		<p>Наименование раздела: 
			<input type='text' name='ru_name' placeholder='Введите название раздела...' class='form-control' aria-describedby='sizing-addon2'>
		</p> 
		</div>
		<div class='col-md-6 col-xs-12'>
		<p><b>TITLE:</b> 
			<input type='text'' name='ru_title' placeholder='Введите заголовок страницы...' class='form-control' aria-describedby='sizing-addon2'>
		</p>
        <div class='block'>
            <p><b>KEYWORDS:</b>
				<input type='text' name='ru_keywords' placeholder='Введите ключевые слова...' class='form-control' aria-describedby='sizing-addon2'>
			</p>
            <p><b>DESCRIPTION:</b>
				<input type='text' name='ru_description' placeholder='Введите описание страницы...' class='form-control' aria-describedby='sizing-addon2'>
			</p>&nbsp;
        </div></div>
        <p><b>Контент страницы:</b><p><textarea id='ru_content_editor' name='ru_content' cols='100' rows='20'></textarea></p>
        <script type='text/javascript'>
            var ckeditor1 = CKEDITOR.replace( 'ru_content_editor' );
            AjexFileManager.init({
                returnTo: 'ckeditor',
                editor: ckeditor1
            });
        </script></p>
        
        <br><h4><b>Страница на английском языке</b></h4>
		<div class='col-md-6 col-xs-12'>
		<p>Name of section: 
			<input type='text' name='en_name' placeholder='Write name section...' class='form-control' aria-describedby='sizing-addon2'>
		</p> 
		</div>
		<div class='col-md-6 col-xs-12'>
		<p><b>Title: </b>
			<input type='text' placeholder='Write title... ' name='en_title' class='form-control' aria-describedby='sizing-addon2'>
		</p>
        <div class='block'>
            <p><b>Keywords: </b>
				<input type='text' placeholder='Write keywords... ' name='en_keywords' class='form-control' aria-describedby='sizing-addon2'>
			</p>
            <p><b>Description: </b>
				<input type='text' placeholder='Write description... ' name='en_description' class='form-control' aria-describedby='sizing-addon2'>
			</p>
        </div></div>
        <p>Text: <textarea id='en_content_editor' name='en_content' cols='100' rows='20'></textarea>
        <script type='text/javascript'>
            var ckeditor1 = CKEDITOR.replace( 'en_content_editor' );
            AjexFileManager.init({
                returnTo: 'ckeditor',
                editor: ckeditor2
            });
        </script></p>&nbsp;
        <p>Опубликовать: <input type='checkbox' name='status' value='1'></p>
		<button class='btn btn-secondary' type='submit' value='Добавить' name='submit' onClick= 'return window.confirm(`Добавить новую страницу?`);'>Добавить</button>

	   
        </form>
        </div>";
    }
    
    static function add_submit()
    {
        $ru_name = $_POST['ru_name'];
		$ru_title = $_POST['ru_title'];
        $ru_keywords = $_POST['ru_keywords'];
        $ru_description = $_POST['ru_description'];
        $ru_content = html_entity_decode($_POST['ru_content']);
        
        
		$en_name = $_POST['en_name'];
		$en_title = $_POST['en_title'];
        $en_keywords = $_POST['en_keywords'];
        $en_description = $_POST['en_description'];
        $en_content = html_entity_decode($_POST['en_content']);
        
        
		$ru_url = CMS::translit($ru_name);
        $en_url = CMS::translit($en_name);
        
		$status = $_POST['status'];
        
        global $DB;
        $sql = $DB->prepare("INSERT INTO history_company (ru_name, ru_title, ru_keywords, ru_description, ru_content, en_name, en_title, en_keywords, en_description, en_content, status, ru_url, en_url) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)");
        $sql->bind_param('sssssssssssss', $ru_name, $ru_title, $ru_keywords, $ru_description, $ru_content, $en_name, $en_title, $en_keywords, $en_description, $en_content, $status, $ru_url, $en_url);
        $sql->execute();
        $result = $sql->get_result();
        header("Location:/admin/?module=history");
    }
	
	static function edit_form($id)
    {
        global $DB;
        $temp = "";
        $sql = $DB->prepare("SELECT * FROM history_company WHERE id=?");
        $sql->bind_param('s', $id);
        $sql->execute();
        $result = $sql->get_result();
        while ($row = $result->fetch_assoc()) 
        {
            $id = $row['id'];
            
            $ru_name = $row['ru_name'];
			$ru_title = $row['ru_title'];
            $ru_keywords = $row['ru_keywords'];
            $ru_description = $row['ru_description'];
			$ru_content = html_entity_decode($row['ru_content']);
            
            $en_name = $row['en_name'];
			$en_title = $row['en_title'];
            $en_keywords = $row['en_keywords'];
            $en_description = $row['en_description'];
			$en_content = html_entity_decode($row['en_content']);
            
            $status = $row['status'];
        }
        $temp = "<div class='forma'>
        <form action='/admin/?module=history&action=edit' method='POST'>
        <input type='hidden' name='id' value='$id'>
          <button type='button' class='btn btn-danger'><a href='/admin/?module=history&action=delete&id=$id' onClick= 'return window.confirm(`Вы уверены, что хотите удалить раздел?`);'><span>Удалить раздел</span></a></button>
        <h3>Редактирование раздела</h3><br />
		<h4><b>Страница на русском языке</b></h4>
        <div class='col-md-6 col-xs-12'>
		<p>Название секции: 
			<input type='text' name='ru_name' value='$ru_name' class='form-control' aria-describedby='sizing-addon2'>
		</p> 
		</div>
		<div class='col-md-6 col-xs-12'>
        <div class='block'>
            <p><b>TITLE: </b>
				<input type='text' name='ru_title' value='$ru_title' class='form-control' aria-describedby='sizing-addon2'>
			</p>
            <p><b>KEYWORDS: </b>
				<input type='text' name='ru_keywords' value='$ru_keywords' class='form-control' aria-describedby='sizing-addon2'>
			</p>
            <p><b>DESCRIPTION: </b>
				<input type='text' name='ru_description' value='$ru_description' class='form-control' aria-describedby='sizing-addon2'>
			</p>
        </div></div>
		<p>Контент страницы: <textarea id='ru_content_editor' name='ru_content' cols='100' rows='20'>$ru_content</textarea>
        <script type='text/javascript'>
            var ckeditor1 = CKEDITOR.replace( 'ru_content_editor' );
            AjexFileManager.init({
                returnTo: 'ckeditor',
                editor: ckeditor1
            });
        </script></p><br />
        
        <h4><b>Страница на английском языке</b></h4>
        <div class='col-md-6 col-xs-12'>
		<p>Name of page: 
			<input type='text' name='en_name' value='$en_name' class='form-control' aria-describedby='sizing-addon2'>
		</p>
		</div>
		<div class='col-md-6 col-xs-12'>
        <div class='block'>
            <p><b>Title: </b> 
				<input type='text' name='en_title' value='$en_title' class='form-control' aria-describedby='sizing-addon2'>
			</p>
            <p><b>Keywords: </b>
				<input type='text' name='en_keywords' value='$en_keywords' class='form-control' aria-describedby='sizing-addon2'>
			</p>
            <p><b>Description: </b>
				<input type='text' name='en_description' value='$en_description' class='form-control' aria-describedby='sizing-addon2'>
			</p>
        </div></div>
		<p>Cpntent page: <textarea id='en_content_editor' name='en_content' cols='100' rows='20'>$en_content</textarea>
        <script type='text/javascript'>
            var ckeditor1 = CKEDITOR.replace( 'en_content_editor' );
            AjexFileManager.init({
                returnTo: 'ckeditor',
                editor: ckeditor1
            });
        </script></p><br />&nbsp";
        if($status =='1')
        {
            $temp .= "<p>Опубликован: <input type='checkbox' name='status' value='1' checked></p>";
        }
        else
        {
            $temp .= "<p>Опубликован: <input type='checkbox' name='status' value='1'></p>";
        }
        $temp .= "<button class='btn btn-secondary' type='submit' value='Изменить' name='submit' onClick= 'return window.confirm(`Изменить раздел??`);'>Изменить</button>
        </form>
        </div>";
        return $temp;
    }
    
    static function edit_submit()
    { 
        $id = $_POST['id'];
            
            $ru_name = $_POST['ru_name'];
			$ru_title = $_POST['ru_title'];
            $ru_keywords = $_POST['ru_keywords'];
            $ru_description = $_POST['ru_description'];
			$ru_content = html_entity_decode($_POST['ru_content']);
            
            $en_name = $_POST['en_name'];
			$en_title = $_POST['en_title'];
            $en_keywords = $_POST['en_keywords'];
            $en_description = $_POST['en_description'];
			$en_content = html_entity_decode($_POST['en_content']);
              
            $status = $_POST['status'];
            
            $ru_url = CMS::translit($ru_name);
            $en_url = CMS::translit($en_name);
       
        global $DB;
        $sql = $DB->prepare("UPDATE history_company SET  ru_name=?, ru_title=?, ru_keywords=?, ru_description=?, ru_content=?, en_name=?, en_title=?,  en_keywords=?, en_description=?, en_content=?, status=?, ru_url=?, en_url=? WHERE id=?");
        $sql->bind_param('ssssssssssssss', $ru_name, $ru_title,  $ru_keywords, $ru_description, $ru_content,  $en_title, $en_name, $en_keywords, $en_description, $en_content, $status, $ru_url, $en_url, $id);
        $sql->execute();
		//var_dump($DB);
        $result = $sql->get_result();
        header("Location:/admin/?module=history");
    }
    
    static function delete($id)
    {
        global $DB;
        $sql = $DB->prepare("DELETE FROM history_company WHERE id=?");
        $sql->bind_param('s', $id);
        $sql->execute();
        header("Location:/admin/?module=history");
    }	
	
}



#####################################################################
//Модуль руководство предприятия
#####################################################################
class admin_leadership{
	
	static function get_list()
    {
        global $DB;
        $temp = "<button type='button' class='btn btn-success'><i class='fa fa-plus-circle' aria-hidden='true' style='margin-right:15px;'></i><a href='/admin/?module=leadership&action=add'>Добавить страницу</a></button>
		<table class='table table-hover'>
        <thead class='thead-dark'>
        <tr>
		<th>Название секции</th>
		<th>Должность</th>
		<th>ФИО</th>
		<th>Срок работы</th>
		<th>Направление работы</th>
        <th>Дополнительная информация</th>
        <th>Статус публикации на странице</th>
        </tr>
        </thead>";
        $sql = $DB->prepare("SELECT id, ru_name as name, ru_fio as fio, ru_years_work as years_work, ru_direction as direction, ru_content as content, status FROM leadership");
        $sql->execute();
        $result = $sql->get_result();
        while ($row = $result->fetch_assoc()) 
        {
            if($row['status'] == 1)
            {
                $row['status'] = 'Опубликована';
            }
            else
            {
                $row['status'] = 'Не опубликована';
            }
            $temp .= "<tr onclick='window.location.href=`/admin/?module=leadership&action=edit&id=$row[id]`; return false'>
						<td>$row[name]</td>
						<td>$row[fio]</td>
						<td>$row[years_work]</td>
						<td>$row[direction]</td>
						<td>$row[content]</td>
                        <td>$row[status]</td>
					</tr>";
        }
        $sql->close();
        $temp .= "</table>";
        return $temp;
		return "";
    }
    
    static function add_form()
    {
        return "<div class='forma'>
        <form action='/admin/?module=leadership&action=add' method='POST'>
        <h3>Добавление новой страницы</h3><br />
		<h4><b>Страница на русском языке</b></h4><br />
		<p>Название секции: 
			<input type='text' name='ru_name' placeholder='Введите название секции...' class='form-control' aria-describedby='sizing-addon2'>
		</p> 
		<p>ФИО: 
			<input type='text' name='ru_fio' placeholder='Введите ФИО...' class='form-control' aria-describedby='sizing-addon2'>
		</p> 
		<p><b>Title:</b> 
			<input type='text'' name='ru_title' placeholder='Введите заголовок страницы...' class='form-control' aria-describedby='sizing-addon2'>
		</p>
        <div class='block'>
            <p><b>Keywords:</b>
				<input type='text' name='ru_keywords' placeholder='Введите ключевые слова...' class='form-control' aria-describedby='sizing-addon2'>
			</p>
            <p><b>Description:</b>
				<input type='text' name='ru_description' placeholder='Введите описание страницы...' class='form-control' aria-describedby='sizing-addon2'>
			</p>&nbsp;
        </div>
		<p>Срок работы: 
			<input type='text' name='ru_years_work' placeholder='Введите название раздела...' class='form-control' aria-describedby='sizing-addon2'>
		</p> 
		<p>Направление работы: 
			<input type='text' name='ru_direction' placeholder='Введите название раздела...' class='form-control' aria-describedby='sizing-addon2'>
		</p> 
		<p><b>Контент страницы:</b><p><textarea id='ru_content_editor' name='ru_content' cols='100' rows='20'></textarea></p>
        <script type='text/javascript'>
            var ckeditor1 = CKEDITOR.replace( 'ru_content_editor' );
            AjexFileManager.init({
                returnTo: 'ckeditor',
                editor: ckeditor1
            });
        </script></p>
        
        <br><h4><b>Страница на английском языке</b></h4>
		<p>Name of section: 
			<input type='text' name='en_name' placeholder='Write name section...' class='form-control' aria-describedby='sizing-addon2'>
		</p> 
		<p>FIO: 
			<input type='text' name='en_fio' placeholder='Write FIO...' class='form-control' aria-describedby='sizing-addon2'>
		</p> 
		<p><b>Title:</b> 
			<input type='text'' name='en_title' placeholder='Write title...' class='form-control' aria-describedby='sizing-addon2'>
		</p>
        <div class='block'>
            <p><b>Keywords:</b>
				<input type='text' name='en_keywords' placeholder='Write keywords...' class='form-control' aria-describedby='sizing-addon2'>
			</p>
            <p><b>Description:</b>
				<input type='text' name='en_description' placeholder='Write description...' class='form-control' aria-describedby='sizing-addon2'>
			</p>&nbsp;
        </div>
		<p>How many work: 
			<input type='text' name='en_years_work' placeholder='Write how many work..' class='form-control' aria-describedby='sizing-addon2'>
		</p> 
		<p>Direction: 
			<input type='text' name='en_direction' placeholder='Write direction...' class='form-control' aria-describedby='sizing-addon2'>
		</p> 
		<p><b>Content of section:</b><p><textarea id='en_content_editor' name='en_content' cols='100' rows='20'></textarea></p>
        <script type='text/javascript'>
            var ckeditor1 = CKEDITOR.replace( 'en_content_editor' );
            AjexFileManager.init({
                returnTo: 'ckeditor',
                editor: ckeditor1
            });
        </script></p>&nbsp;
        <p>Опубликовать: <input type='checkbox' name='status' value='1'></p>
		<button class='btn btn-secondary' type='submit' value='Добавить' name='submit' onClick= 'return window.confirm(`Добавить новую страницу?`);'>Добавить</button>

	   
        </form>
        </div>";
    }
    
    static function add_submit()
    {
        $ru_name = $_POST['ru_name'];
		$ru_fio = $_POST['ru_fio'];
		$ru_title = $_POST['ru_title'];
        $ru_keywords = $_POST['ru_keywords'];
        $ru_description = $_POST['ru_description'];
		$ru_years_work = $_POST['ru_years_work'];
		$ru_direction = $_POST['ru_direction'];
        $ru_content = html_entity_decode($_POST['ru_content']);
        
        
		$en_name = $_POST['en_name'];
		$en_fio = $_POST['en_fio'];
		$en_title = $_POST['en_title'];
        $en_keywords = $_POST['en_keywords'];
        $en_description = $_POST['en_description'];
		$en_years_work = $_POST['en_years_work'];
		$en_direction = $_POST['en_direction'];
        $en_content = html_entity_decode($_POST['en_content']);
        
        
		$ru_url = CMS::translit($ru_name);
        $en_url = CMS::translit($en_name);
        
		
        
        global $DB;
        $sql = $DB->prepare("INSERT INTO leadership (ru_name, ru_fio, ru_title, ru_keywords, ru_description, ru_years_work, ru_direction, ru_content, en_name, en_fio, en_title, en_keywords, en_description, en_years_work, en_direction, en_content, status, ru_url, en_url) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
        $sql->bind_param('sssssssssssssssssss', $ru_name, $ru_fio, $ru_title, $ru_keywords, $ru_description, $ru_years_work, $ru_direction, $ru_content, $en_name, $en_fio, $en_title, $en_keywords, $en_description, $en_years_work, $en_direction, $en_content, $status, $ru_url, $en_url);
        $sql->execute();
        $result = $sql->get_result();
        header("Location:/admin/?module=leadership");
    }	
	

}

#####################################################################
//Модуль контактов
#####################################################################
class admin_contacts{
	
	static function get_list()
    {
		 global $DB;
        $temp = "<button type='button' class='btn btn-success'><i class='fa fa-plus-circle' aria-hidden='true' style='margin-right:15px;'></i><a href='/admin/?module=contacts&action=add'>Добавить контактную информацию</a></button>
		<table class='table table-hover'>
        <thead class='thead-dark'>
        <tr>
		<th>id</th>
		<th>Наименование</th>
		<th>Адрес</th>
		<th>Телефон</th>
		<th>E-mail</th>
		<th>Время работы</th>
		<th>Статус публикации на странице</th>
        </tr>
        </thead>";
		 $sql = $DB->prepare("SELECT id, ru_name as name, ru_address as address, ru_phone as phone, ru_e_mail as email, ru_time_of_work as time_of_work, status FROM contacts");
        $sql->execute();
        $result = $sql->get_result();
		  while ($row = $result->fetch_assoc()) 
        {
			$class = '';
			if($row['status'] == 1)
			{
				$class="admin_published";
			}
			else{
				$class="admin_published1";
			}
			
            if($row['status'] == 1)
            {
                $row['status'] = 'Опубликована';
            }
            else
            {
                $row['status'] = 'Не опубликована';
            }
            $temp .= "<tr class='".$class."' onclick='window.location.href=`/admin/?module=contacts&action=edit&id=$row[id]`; return false'>
						<td>$row[id]</td>
						<td>$row[name]</td>
						<td>$row[address]</td>
						<td>$row[phone]</td>
					</tr>";
        }
        $sql->close();
        $temp .= "</table>";
        return $temp;
		return "";
	}
	
	static function add_form()
    {
        $temp = "<div class='forma'>
        <form action='/admin/?module=contacts&action=add' method='POST'>
        <h3>Добавление контактной информации</h3><br />
		<h4><b>Страница на русском языке</b></h4>
		 <p>Наименование: 
			<input type='text' placeholder='Введите название...' name='ru_name' class='form-control' aria-describedby='sizing-addon2'>
		</p>
		<div class='col-md-6 col-xs-12'>
	   <p>Адрес: 
			<input type='text' placeholder='Введите адрес...' name='ru_address' class='form-control' aria-describedby='sizing-addon2'>
		</p> 
		<p>Телефон: 
			<input type='text' name='ru_phone' placeholder='Введите телефон...' class='form-control' aria-describedby='sizing-addon2'>
		</p>
		<p>E-mail: 
			<input type='text' name='ru_e_mail' placeholder='Введите e-mail...' class='form-control' aria-describedby='sizing-addon2'>
		</p>
		</div>
		<div class='col-md-6 col-xs-12'>
        <div class='block'>
            <p><b>TITLE: </b>
				<input type='text' name='ru_title' placeholder='Введите заголовок...' class='form-control' aria-describedby='sizing-addon2'>
			</p>
            <p><b>KEYWORDS: </b>
				<input type='text' name='ru_keywords' placeholder='Введите ключевые слова...' class='form-control' aria-describedby='sizing-addon2'>
			</p>
            <p><b>DESCRIPTION: </b>
				<input type='text' name='ru_description' placeholder='Введите описане...' class='form-control' aria-describedby='sizing-addon2'>
			</p>
        </div></div>
		<p>Время работы: 
			<input type='text' name='ru_time_of_work' placeholder='Введите время работы' class='form-control' aria-describedby='sizing-addon2'>
		</p>
		<p>Дполонительная контактная информация: <textarea id='ru_content_editor' name='ru_content' cols='100' rows='20'></textarea>
        <script type='text/javascript'>
            var ckeditor1 = CKEDITOR.replace( 'ru_content_editor' );
            AjexFileManager.init({
                returnTo: 'ckeditor',
                editor: ckeditor1
            });
        </script></p>
 		<br />
        
        <h4><b>Страница на английском языке</b></h4>
		 <p>Name: 
			<input type='text' placeholder='Write name...' name='en_name' class='form-control' aria-describedby='sizing-addon2'>
		</p> 
		<div class='col-md-6 col-xs-12'>
	   <p>Address: 
			<input type='text' placeholder='Write address...' name='en_address' class='form-control' aria-describedby='sizing-addon2'>
		</p> 
		<p>Phone: 
			<input type='text' name='en_phone' placeholder='Write phone....' class='form-control' aria-describedby='sizing-addon2'>
		</p>
		<p>E-mail: 
			<input type='text' name='en_e_mail' placeholder='Write e-mail...' class='form-control' aria-describedby='sizing-addon2'>
		</p>
		</div>
		<div class='col-md-6 col-xs-12'>
        <div class='block'>
            <p><b>TITLE: </b>
				<input type='text' name='en_title' placeholder='Write title...' class='form-control' aria-describedby='sizing-addon2'>
			</p>
            <p><b>KEYWORDS: </b>
				<input type='text' name='en_keywords' placeholder='Write keywords...' class='form-control' aria-describedby='sizing-addon2'>
			</p>
            <p><b>DESCRIPTION: </b>
				<input type='text' name='en_description' placeholder='Write description...' class='form-control' aria-describedby='sizing-addon2'>
			</p>
        </div></div>
		<p>Time of work: 
			<input type='text' name='en_time_of_work' placeholder='Write time of work...' class='form-control' aria-describedby='sizing-addon2'>
		</p>
		<p>Another contact information: <textarea id='en_content_editor' name='en_content' cols='100' rows='20'></textarea>
        <script type='text/javascript'>
            var ckeditor1 = CKEDITOR.replace( 'en_content_editor' );
            AjexFileManager.init({
                returnTo: 'ckeditor',
                editor: ckeditor1
            });
        </script></p>
 		<br />
        
		<p>Опубликовать: <input type='checkbox' name='status' value='1'></p>
        <button class='btn btn-secondary' type='submit' value='Добавить' name='submit' onClick= 'return window.confirm(`Добавить новую новость??`);'>Добавить</button>
        </form>
        </div>";
		return $temp;
    }
	
	static function add_submit()
    {
		
		 
		$ru_name = $_POST['ru_name'];
        $ru_address = $_POST['ru_address'];
		$ru_phone = $_POST['ru_phone'];
		$ru_e_mail = $_POST['ru_e_mail'];
		$ru_title = $_POST['ru_title'];
        $ru_keywords = $_POST['ru_keywords'];
        $ru_description = $_POST['ru_description'];
		$ru_time_of_work = $_POST['ru_time_of_work'];
        $ru_content = html_entity_decode($_POST['ru_content']);
        
        $en_name = $_POST['en_name'];
		$en_address = $_POST['en_address'];
		$en_phone = $_POST['en_phone'];
		$en_e_mail = $_POST['en_e_mail'];
		$en_title = $_POST['en_title'];
        $en_keywords = $_POST['en_keywords'];
        $en_description = $_POST['en_description'];
		$en_time_of_work = $_POST['en_time_of_work'];
        $en_content = html_entity_decode($_POST['en_content']);
        
        
		$ru_url = CMS::translit($ru_name);
        $en_url = CMS::translit($en_name);
        
		$status = $_POST['status'];
        
        global $DB;
        $sql = $DB->prepare("INSERT INTO contacts (ru_name, ru_address, ru_phone, ru_e_mail, ru_title, ru_keywords, ru_description, ru_time_of_work, ru_content, en_name, en_address, en_phone, en_e_mail, en_title, en_keywords, en_description, en_time_of_work, en_content, status, ru_url, en_url) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
        if(!$sql) echo($DB->error);
		$sql->bind_param('sssssssssssssssssssss', $ru_name, $ru_address, $ru_phone, $ru_e_mail, $ru_title, $ru_keywords, $ru_description, $ru_time_of_work, $ru_content, $en_name, $en_address, $en_phone, $en_e_mail, $en_title, $en_keywords, $en_description, $en_time_of_work, $en_content, $status, $ru_url, $en_url);
        $sql->execute();
        $result = $sql->get_result();
       header("Location:/admin/?module=contacts");
    }	
		
	static function edit_form($id)
    {
        global $DB;
        $temp = "";
        $sql = $DB->prepare("SELECT * FROM contacts WHERE id=?");
        $sql->bind_param('s', $id);
        $sql->execute();
        $result = $sql->get_result();
        while ($row = $result->fetch_assoc()) 
        {
            $id = $row['id'];
            
			$ru_name = $row['ru_name'];
			$ru_address = $row['ru_address'];
			$ru_phone = $row['ru_phone'];
            $ru_e_mail = $row['ru_e_mail'];
            $ru_title = $row['ru_title'];
			$ru_keywords = $row['ru_keywords'];
            $ru_description = $row['ru_description'];
			$ru_time_of_work = $row['ru_time_of_work'];
            $ru_content = html_entity_decode($row['ru_content']);
          
            
            
            $en_name = $row['en_name'];
			$en_address = $row['en_address'];
			$en_phone = $row['en_phone'];
            $en_e_mail = $row['en_e_mail'];
            $en_title = $row['en_title'];
			$en_keywords = $row['en_keywords'];
            $en_description = $row['en_description'];
			$en_time_of_work = $row['en_time_of_work'];
            $en_content = html_entity_decode($row['en_content']);
			
			
            $status = $row['status'];
          
        }
        $temp = "<div class='forma'>
        <form action='/admin/?module=contacts&action=edit' method='POST'>
        <input type='hidden' name='id' value='$id'>
		  <button type='button' class='btn btn-danger'><a href='/admin/?module=contacts&action=delete&id=$id' onClick= 'return window.confirm(`Вы уверены, что хотите удалить страницу?`);'><span>Удалить страницу</span></a></button>
		<h3>Редактирование контактной информации</h3><br />
		<br><h4><b>Страница на руссском языке</b></h4>
		<p>Наpdfybt: 
			<input type='text' name='ru_name' value='$ru_name' class='form-control' aria-describedby='sizing-addon2'>
		</p>
		<div class='col-md-6 col-xs-12'>
		<p>Адрес: 
			<input type='text' name='ru_address' value='$ru_address' class='form-control' aria-describedby='sizing-addon2'>
		</p> 
		<p>Телефон: 
			<input type='text' name='ru_phone'  value='$ru_phone' class='form-control' aria-describedby='sizing-addon2'>
		</p>
		<p>E-mail: 
			<input type='text' name='ru_e_mail'  value='$ru_e_mail' class='form-control' aria-describedby='sizing-addon2'>
		</p>
		</div>
		<div class='col-md-6 col-xs-12'>
        <div class='block'>
            <p><b>TITLE: </b>
				<input type='text' name='ru_title'  value='$ru_title' class='form-control' aria-describedby='sizing-addon2'>
			</p>
            <p><b>KEYWORDS: </b>
				<input type='text' name='ru_keywords'  value='$ru_keywords' class='form-control' aria-describedby='sizing-addon2'>
			</p>
            <p><b>DESCRIPTION: </b>
				<input type='text' name='ru_description'  value='$ru_description' class='form-control' aria-describedby='sizing-addon2'>
			</p>
        </div></div>
		<p>Время работы: 
			<input type='text' name='ru_time_of_work'  value='$ru_time_of_work' class='form-control' aria-describedby='sizing-addon2'>
		</p>
        <p>Дополнительная контактная информация: <textarea id='ru_content_editor' name='ru_content' cols='100' rows='20'>$ru_content</textarea>
        <script type='text/javascript'>
            var ckeditor1 = CKEDITOR.replace( 'ru_content_editor' );
            AjexFileManager.init({
                returnTo: 'ckeditor',
                editor: ckeditor1
            });
        </script></p>
 		<br /><br />
        
         <br><h4><b>Страница на английском языке</b></h4>
        <p>Name of organization: 
			<input type='text' name='en_name' value='$en_name' class='form-control' aria-describedby='sizing-addon2'>
		</p>
		<div class='col-md-6 col-xs-12'>
		<p>Address: 
			<input type='text' name='en_address' value='$en_address' class='form-control' aria-describedby='sizing-addon2'>
		</p>
		<p>Phone: 
			<input type='text' name='en_phone'  value='$en_phone' class='form-control' aria-describedby='sizing-addon2'>
		</p>
		<p>E-mail: 
			<input type='text' name='en_e_mail'  value='$en_e_mail' class='form-control' aria-describedby='sizing-addon2'>
		</p>
		</div>
		<div class='col-md-6 col-xs-12'>
        <div class='block'>
            <p><b>Title: </b>
				<input type='text' name='en_title' value='$en_title' class='form-control' aria-describedby='sizing-addon2'>
			</p>
            <p><b>Keywords: </b>
				<input type='text' name='en_keywords'  value='$en_keywords' class='form-control' aria-describedby='sizing-addon2'>
			</p>
            <p><b>Description: </b>
				<input type='text' name='en_description' p value='$en_description' class='form-control' aria-describedby='sizing-addon2'>
			</p>
        </div>
		</div>
		<p>Time of work: 
			<input type='text' name='en_time_of_work'  value='$en_time_of_work' class='form-control' aria-describedby='sizing-addon2'>
		</p>
        <p>Another contact information: <textarea id='en_content_editor' name='en_content' cols='100' rows='20'>$en_content</textarea>
        <script type='text/javascript'>
            var ckeditor1 = CKEDITOR.replace( 'en_content_editor' );
            AjexFileManager.init({
                returnTo: 'ckeditor',
                editor: ckeditor1
            });
        </script></p>
		&nbsp;";
       if($status =='1')
        {
            $temp .= "<p>Опубликовать: <input type='checkbox' name='status' value='1' checked></p>";
        }
        else
        {
            $temp .= "<p>Опубликовано: <input type='checkbox' name='status' value='1'></p>";
        }
        $temp .= "
		<button class='btn btn-secondary' type='submit' value='Изменить' name='submit' onClick= 'return window.confirm(`Вы уверены, что хотите применить изменения к этой странице?`);'>Изменить</button>
        </form>
        </div>";
        return $temp;
    }
	
	static function edit_submit()
    { 
        $id = $_POST['id'];
            
            $ru_name = $_POST['ru_name'];
			$ru_address = $_POST['ru_address'];
			$ru_phone = $_POST['ru_phone'];
			$ru_e_mail = $_POST['ru_e_mail'];
			$ru_title = $_POST['ru_title'];
			$ru_keywords = $_POST['ru_keywords'];
			$ru_description = $_POST['ru_description'];
			$ru_time_of_work = $_POST['ru_time_of_work'];
			$ru_content = html_entity_decode($_POST['ru_content']);
			
        
			$en_name = $_POST['en_name'];
			$en_address = $_POST['en_address'];
			$en_phone = $_POST['en_phone'];
			$en_e_mail = $_POST['en_e_mail'];
			$en_title = $_POST['en_title'];
			$en_keywords = $_POST['en_keywords'];
			$en_description = $_POST['en_description'];
			$en_time_of_work = $_POST['en_time_of_work'];
			$en_content = html_entity_decode($_POST['en_content']);
			
			  
            $status = $_POST['status'];
			
			$ru_url = CMS::translit($ru_name);
            $en_url = CMS::translit($en_name);
            
            
        global $DB;
        $sql = $DB->prepare("UPDATE contacts SET ru_name=?, ru_address=?, ru_phone=?, ru_e_mail=?, ru_title=?, ru_keywords=?, ru_description=?, ru_time_of_work=?, ru_content=?, en_name=?, en_address=?, en_phone=?, en_e_mail=?, en_title=?, en_keywords=?, en_description=?, en_time_of_work=?, en_content=?, ru_url=?, en_url=?, status=? WHERE id=?");
         if(!$sql) echo($DB->error);
		$sql->bind_param('ssssssssssssssssssssss', $ru_name, $ru_address, $ru_phone, $ru_e_mail, $ru_title, $ru_keywords, $ru_description, $ru_time_of_work, $ru_content, $en_name,$en_address, $en_phone, $en_e_mail, $en_title, $en_keywords, $en_description, $en_time_of_work, $en_content, $ru_url, $en_url, $status, $id);
        $sql->execute();
        $result = $sql->get_result();
		
     header("Location:/admin/?module=contacts");
    }
	
	 static function delete($id)
    {
        global $DB;
        $sql = $DB->prepare("DELETE FROM contacts WHERE id=?");
        $sql->bind_param('s', $id);
        $sql->execute();
        header("Location:/admin/?module=contacts");
    }	
	
}

#####################################################################
//Модуль контактов
#####################################################################
class admin_events{
	
	static function get_list()
    {
		 global $DB;
        $temp = "<button type='button' class='btn btn-success'><i class='fa fa-plus-circle' aria-hidden='true' style='margin-right:15px;'></i><a href='/admin/?module=events&action=add'>Добавить информацию о проведении мероприятий</a></button>
		<table class='table table-hover'>
        <thead class='thead-dark'>
        <tr>
		<th>id</th>
		<th>Наименование</th>
		<th>Адрес</th>
		<th>Телефон</th>
		<th>E-mail</th>
        </tr>
        </thead>";
		 $sql = $DB->prepare("SELECT id, ru_name as name, ru_address as address, ru_phone as phone, ru_e_mail as email status FROM events");
        $sql->execute();
        $result = $sql->get_result();
		  while ($row = $result->fetch_assoc()) 
        {
			$class = '';
			if($row['status'] == 1)
			{
				$class="admin_published";
			}
			else{
				$class="admin_published1";
			}
			
            if($row['status'] == 1)
            {
                $row['status'] = 'Опубликована';
            }
            else
            {
                $row['status'] = 'Не опубликована';
            }
            $temp .= "<tr class='".$class."' onclick='window.location.href=`/admin/?module=events&action=edit&id=$row[id]`; return false'>
						<td>$row[id]</td>
						<td>$row[name]</td>
						<td>$row[address]</td>
						<td>$row[phone]</td>
						<td>$row[email]</td>
						<td>$row[status]</td>
					</tr>";
        }
        $sql->close();
        $temp .= "</table>";
        return $temp;
		return "";
	}
	
	static function add_form()
    {
        $temp = "<div class='forma'>
        <form action='/admin/?module=events&action=add' method='POST'>
        <h3>Добавление контактной информации</h3><br />
		<h4><b>Страница на русском языке</b></h4>
		 <p>Наименование: 
			<input type='text' placeholder='Введите название...' name='ru_name' class='form-control' aria-describedby='sizing-addon2'>
		</p>
		<div class='col-md-6 col-xs-12'>
	   <p>Адрес: 
			<input type='text' placeholder='Введите адрес...' name='ru_address' class='form-control' aria-describedby='sizing-addon2'>
		</p> 
		<p>Телефон: 
			<input type='text' name='ru_phone' placeholder='Введите телефон...' class='form-control' aria-describedby='sizing-addon2'>
		</p>
		<p>E-mail: 
			<input type='text' name='ru_e_mail' placeholder='Введите e-mail...' class='form-control' aria-describedby='sizing-addon2'>
		</p>
		</div>
		<div class='col-md-6 col-xs-12'>
        <div class='block'>
            <p><b>TITLE: </b>
				<input type='text' name='ru_title' placeholder='Введите заголовок...' class='form-control' aria-describedby='sizing-addon2'>
			</p>
            <p><b>KEYWORDS: </b>
				<input type='text' name='ru_keywords' placeholder='Введите ключевые слова...' class='form-control' aria-describedby='sizing-addon2'>
			</p>
            <p><b>DESCRIPTION: </b>
				<input type='text' name='ru_description' placeholder='Введите описане...' class='form-control' aria-describedby='sizing-addon2'>
			</p>
        </div></div>
		<p>Время работы: 
			<input type='text' name='ru_time_of_work' placeholder='Введите время работы' class='form-control' aria-describedby='sizing-addon2'>
		</p>
 		<br />
        
        <h4><b>Страница на английском языке</b></h4>
		 <p>Name: 
			<input type='text' placeholder='Write name...' name='en_name' class='form-control' aria-describedby='sizing-addon2'>
		</p> 
		<div class='col-md-6 col-xs-12'>
	   <p>Address: 
			<input type='text' placeholder='Write address...' name='en_address' class='form-control' aria-describedby='sizing-addon2'>
		</p> 
		<p>Phone: 
			<input type='text' name='en_phone' placeholder='Write phone....' class='form-control' aria-describedby='sizing-addon2'>
		</p>
		<p>E-mail: 
			<input type='text' name='en_e_mail' placeholder='Write e-mail...' class='form-control' aria-describedby='sizing-addon2'>
		</p>
		</div>
		<div class='col-md-6 col-xs-12'>
        </div>
		<p>Time of work: 
			<input type='text' name='en_time_of_work' placeholder='Write time of work...' class='form-control' aria-describedby='sizing-addon2'>
		</p>
		<p>Another contact information: <textarea id='en_content_editor' name='en_content' cols='100' rows='20'></textarea>
        <script type='text/javascript'>
            var ckeditor1 = CKEDITOR.replace( 'en_content_editor' );
            AjexFileManager.init({
                returnTo: 'ckeditor',
                editor: ckeditor1
            });
        </script></p>
 		<br />
        
		<p>Опубликовать: <input type='checkbox' name='status' value='1'></p>
        <button class='btn btn-secondary' type='submit' value='Добавить' name='submit' onClick= 'return window.confirm(`Добавить новую новость??`);'>Добавить</button>
        </form>
        </div>";
		return $temp;
    }
	
	static function add_submit()
    {
		
		 
		$ru_name = $_POST['ru_name'];
        $ru_address = $_POST['ru_address'];
		$ru_phone = $_POST['ru_phone'];
		$ru_e_mail = $_POST['ru_e_mail'];
		$ru_title = $_POST['ru_title'];
        $ru_keywords = $_POST['ru_keywords'];
        $ru_description = $_POST['ru_description'];
        $ru_content = html_entity_decode($_POST['ru_content']);
        
        $en_name = $_POST['en_name'];
		$en_address = $_POST['en_address'];
		$en_phone = $_POST['en_phone'];
		$en_e_mail = $_POST['en_e_mail'];
		$en_title = $_POST['en_title'];
        $en_keywords = $_POST['en_keywords'];
        $en_description = $_POST['en_description'];
		$en_time_of_work = $_POST['en_time_of_work'];
        $en_content = html_entity_decode($_POST['en_content']);
        
        
		$ru_url = CMS::translit($ru_name);
        $en_url = CMS::translit($en_name);
        
		$status = $_POST['status'];
        
        global $DB;
        $sql = $DB->prepare("INSERT INTO contacts (ru_name, ru_address, ru_phone, ru_e_mail, ru_title, ru_keywords, ru_description, ru_time_of_work, ru_content, en_name, en_address, en_phone, en_e_mail, en_title, en_keywords, en_description, en_time_of_work, en_content, status, ru_url, en_url) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
        if(!$sql) echo($DB->error);
		$sql->bind_param('sssssssssssssssssssss', $ru_name, $ru_address, $ru_phone, $ru_e_mail, $ru_title, $ru_keywords, $ru_description, $ru_time_of_work, $ru_content, $en_name, $en_address, $en_phone, $en_e_mail, $en_title, $en_keywords, $en_description, $en_time_of_work, $en_content, $status, $ru_url, $en_url);
        $sql->execute();
        $result = $sql->get_result();
       header("Location:/admin/?module=contacts");
    }	
		
	static function edit_form($id)
    {
        global $DB;
        $temp = "";
        $sql = $DB->prepare("SELECT * FROM contacts WHERE id=?");
        $sql->bind_param('s', $id);
        $sql->execute();
        $result = $sql->get_result();
        while ($row = $result->fetch_assoc()) 
        {
            $id = $row['id'];
            
			$ru_name = $row['ru_name'];
			$ru_address = $row['ru_address'];
			$ru_phone = $row['ru_phone'];
            $ru_e_mail = $row['ru_e_mail'];
            $ru_title = $row['ru_title'];
			$ru_keywords = $row['ru_keywords'];
            $ru_description = $row['ru_description'];
			$ru_time_of_work = $row['ru_time_of_work'];
            $ru_content = html_entity_decode($row['ru_content']);
          
            
            
            $en_name = $row['en_name'];
			$en_address = $row['en_address'];
			$en_phone = $row['en_phone'];
            $en_e_mail = $row['en_e_mail'];
            $en_title = $row['en_title'];
			$en_keywords = $row['en_keywords'];
            $en_description = $row['en_description'];
			$en_time_of_work = $row['en_time_of_work'];
            $en_content = html_entity_decode($row['en_content']);
			
			
            $status = $row['status'];
          
        }
        $temp = "<div class='forma'>
        <form action='/admin/?module=contacts&action=edit' method='POST'>
        <input type='hidden' name='id' value='$id'>
		  <button type='button' class='btn btn-danger'><a href='/admin/?module=contacts&action=delete&id=$id' onClick= 'return window.confirm(`Вы уверены, что хотите удалить страницу?`);'><span>Удалить страницу</span></a></button>
		<h3>Редактирование контактной информации</h3><br />
		<br><h4><b>Страница на руссском языке</b></h4>
		<p>Наpdfybt: 
			<input type='text' name='ru_name' value='$ru_name' class='form-control' aria-describedby='sizing-addon2'>
		</p>
		<div class='col-md-6 col-xs-12'>
		<p>Адрес: 
			<input type='text' name='ru_address' value='$ru_address' class='form-control' aria-describedby='sizing-addon2'>
		</p> 
		<p>Телефон: 
			<input type='text' name='ru_phone'  value='$ru_phone' class='form-control' aria-describedby='sizing-addon2'>
		</p>
		<p>E-mail: 
			<input type='text' name='ru_e_mail'  value='$ru_e_mail' class='form-control' aria-describedby='sizing-addon2'>
		</p>
		</div>
		<div class='col-md-6 col-xs-12'>
        <div class='block'>
            <p><b>TITLE: </b>
				<input type='text' name='ru_title'  value='$ru_title' class='form-control' aria-describedby='sizing-addon2'>
			</p>
            <p><b>KEYWORDS: </b>
				<input type='text' name='ru_keywords'  value='$ru_keywords' class='form-control' aria-describedby='sizing-addon2'>
			</p>
            <p><b>DESCRIPTION: </b>
				<input type='text' name='ru_description'  value='$ru_description' class='form-control' aria-describedby='sizing-addon2'>
			</p>
        </div></div>
		<p>Время работы: 
			<input type='text' name='ru_time_of_work'  value='$ru_time_of_work' class='form-control' aria-describedby='sizing-addon2'>
		</p>
        <p>Дополнительная контактная информация: <textarea id='ru_content_editor' name='ru_content' cols='100' rows='20'>$ru_content</textarea>
        <script type='text/javascript'>
            var ckeditor1 = CKEDITOR.replace( 'ru_content_editor' );
            AjexFileManager.init({
                returnTo: 'ckeditor',
                editor: ckeditor1
            });
        </script></p>
 		<br /><br />
        
         <br><h4><b>Страница на английском языке</b></h4>
        <p>Name of organization: 
			<input type='text' name='en_name' value='$en_name' class='form-control' aria-describedby='sizing-addon2'>
		</p>
		<div class='col-md-6 col-xs-12'>
		<p>Address: 
			<input type='text' name='en_address' value='$en_address' class='form-control' aria-describedby='sizing-addon2'>
		</p>
		<p>Phone: 
			<input type='text' name='en_phone'  value='$en_phone' class='form-control' aria-describedby='sizing-addon2'>
		</p>
		<p>E-mail: 
			<input type='text' name='en_e_mail'  value='$en_e_mail' class='form-control' aria-describedby='sizing-addon2'>
		</p>
		</div>
		<div class='col-md-6 col-xs-12'>
        <div class='block'>
            <p><b>Title: </b>
				<input type='text' name='en_title' value='$en_title' class='form-control' aria-describedby='sizing-addon2'>
			</p>
            <p><b>Keywords: </b>
				<input type='text' name='en_keywords'  value='$en_keywords' class='form-control' aria-describedby='sizing-addon2'>
			</p>
            <p><b>Description: </b>
				<input type='text' name='en_description' p value='$en_description' class='form-control' aria-describedby='sizing-addon2'>
			</p>
        </div>
		</div>
		<p>Time of work: 
			<input type='text' name='en_time_of_work'  value='$en_time_of_work' class='form-control' aria-describedby='sizing-addon2'>
		</p>
        <p>Another contact information: <textarea id='en_content_editor' name='en_content' cols='100' rows='20'>$en_content</textarea>
        <script type='text/javascript'>
            var ckeditor1 = CKEDITOR.replace( 'en_content_editor' );
            AjexFileManager.init({
                returnTo: 'ckeditor',
                editor: ckeditor1
            });
        </script></p>
		&nbsp;";
       if($status =='1')
        {
            $temp .= "<p>Опубликовать: <input type='checkbox' name='status' value='1' checked></p>";
        }
        else
        {
            $temp .= "<p>Опубликовано: <input type='checkbox' name='status' value='1'></p>";
        }
        $temp .= "
		<button class='btn btn-secondary' type='submit' value='Изменить' name='submit' onClick= 'return window.confirm(`Вы уверены, что хотите применить изменения к этой странице?`);'>Изменить</button>
        </form>
        </div>";
        return $temp;
    }
	
	static function edit_submit()
    { 
        $id = $_POST['id'];
            
            $ru_name = $_POST['ru_name'];
			$ru_address = $_POST['ru_address'];
			$ru_phone = $_POST['ru_phone'];
			$ru_e_mail = $_POST['ru_e_mail'];
			$ru_title = $_POST['ru_title'];
			$ru_keywords = $_POST['ru_keywords'];
			$ru_description = $_POST['ru_description'];
			$ru_time_of_work = $_POST['ru_time_of_work'];
			$ru_content = html_entity_decode($_POST['ru_content']);
			
        
			$en_name = $_POST['en_name'];
			$en_address = $_POST['en_address'];
			$en_phone = $_POST['en_phone'];
			$en_e_mail = $_POST['en_e_mail'];
			$en_title = $_POST['en_title'];
			$en_keywords = $_POST['en_keywords'];
			$en_description = $_POST['en_description'];
			$en_time_of_work = $_POST['en_time_of_work'];
			$en_content = html_entity_decode($_POST['en_content']);
			
			  
            $status = $_POST['status'];
			
			$ru_url = CMS::translit($ru_name);
            $en_url = CMS::translit($en_name);
            
            
        global $DB;
        $sql = $DB->prepare("UPDATE contacts SET ru_name=?, ru_address=?, ru_phone=?, ru_e_mail=?, ru_title=?, ru_keywords=?, ru_description=?, ru_time_of_work=?, ru_content=?, en_name=?, en_address=?, en_phone=?, en_e_mail=?, en_title=?, en_keywords=?, en_description=?, en_time_of_work=?, en_content=?, ru_url=?, en_url=?, status=? WHERE id=?");
         if(!$sql) echo($DB->error);
		$sql->bind_param('ssssssssssssssssssssss', $ru_name, $ru_address, $ru_phone, $ru_e_mail, $ru_title, $ru_keywords, $ru_description, $ru_time_of_work, $ru_content, $en_name,$en_address, $en_phone, $en_e_mail, $en_title, $en_keywords, $en_description, $en_time_of_work, $en_content, $ru_url, $en_url, $status, $id);
        $sql->execute();
        $result = $sql->get_result();
		
     header("Location:/admin/?module=contacts");
    }
	
	 static function delete($id)
    {
        global $DB;
        $sql = $DB->prepare("DELETE FROM contacts WHERE id=?");
        $sql->bind_param('s', $id);
        $sql->execute();
        header("Location:/admin/?module=contacts");
    }	
	
}



#####################################################################
//Модуль заявки на заказ
#####################################################################
class admin_zayavka_buy{
	static function get_list()
    {
		global $DB;
        $temp = "
		<table class='table table-hover'>
        <thead class='thead-dark'>
        <tr>
		<th>id</th>
		<th>ФИО</th>
		<th>E-mail</th>
		<th>Тема</th>
        </tr>
        </thead>";
        $sql = $DB->prepare("SELECT id, name, e_mail, sub FROM feedback");
        $sql->execute();
        $result = $sql->get_result();
        while ($row = $result->fetch_assoc()) 
        {
            $temp .= "<tr onclick='window.location.href=`/admin/?module=zayavka_buy&action=edit&id=$row[id]`; return false'>
						<td>$row[id]</td>
						<td>$row[name]</td>
						<td>$row[e_mail]</td>
						<td>$row[sub]</td>
					</tr>";
        }
        $sql->close();
        $temp .= "</table>";
        return $temp;
		return "";
	}
	
    
    static function delete($id)
    {
        global $DB;
        $sql = $DB->prepare("DELETE FROM feedback WHERE id=?");
        $sql->bind_param('s', $id);
        $sql->execute();
        header("Location:/admin/?module=zayavka_buy");
    }	
}


#####################################################################
//Модуль заявки на заказ
#####################################################################
class admin_zayavka_phone{
	static function get_list()
    {
		global $DB;
        $temp = "
		<table class='table table-hover'>
        <thead class='thead-dark'>
        <tr>
		<th>id</th>
		<th>ФИО</th>
		<th>E-mail</th>
		<th>Тема</th>
        </tr>
        </thead>";
        $sql = $DB->prepare("SELECT id, name, e_mail, sub FROM zayavka");
        $sql->execute();
        $result = $sql->get_result();
        while ($row = $result->fetch_assoc()) 
        {
            $temp .= "<tr onclick='window.location.href=`/admin/?module=zayavka_phone&action=edit&id=$row[id]`; return false'>
						<td>$row[id]</td>
						<td>$row[name]</td>
						<td>$row[e_mail]</td>
						<td>$row[sub]</td>
					</tr>";
        }
        $sql->close();
        $temp .= "</table>";
        return $temp;
		return "";
	}
	
    
    static function delete($id)
    {
        global $DB;
        $sql = $DB->prepare("DELETE FROM zayavka WHERE id=?");
        $sql->bind_param('s', $id);
        $sql->execute();
        header("Location:/admin/?module=zayavka_phone");
    }	
}


#####################################################################
//Модуль сертификатов
#####################################################################
class admin_certificates{
	
	static function get_list()
    {
        global $DB;
        $temp = "<button type='button' class='btn btn-success'><i class='fa fa-plus-circle' aria-hidden='true' style='margin-right:15px;'></i><a href='/admin/?module=certificates&action=add'>Добавить сертификат</a></button>
		<table class='table table-hover'>
        <thead class='thead-dark'>
        <tr>
		<th>id</th>
		<th>Название сертификата</th>
		<th>Статус публикации</th>
        </tr>
        </thead>";
        $sql = $DB->prepare("SELECT id, ru_name as name, status FROM certificates");
        $sql->execute();
        $result = $sql->get_result();
        while ($row = $result->fetch_assoc()) 
        {
            if($row['status'] == 1)
            {
                $row['status'] = 'Опубликована';
            }
            else
            {
                $row['status'] = 'Не опубликована';
            }
            $temp .= "<tr onclick='window.location.href=`/admin/?module=certificates&action=edit&id=$row[id]`; return false'>
						<td>$row[id]</td>
						<td>$row[name]</td>
						<td>$row[status]</td>
					</tr>";
        }
        $sql->close();
        $temp .= "</table>";
        return $temp;
		return "";
    }
	
	static function add_form()
    {
        $temp = "<div class='forma'>
        <form action='/admin/?module=certificates&action=add' method='POST' enctype='multipart/form-data'>
        <h3>Добавление сертификата</h3><br />
		<h4><b>Страница на русском языке</b></h4>
		<div class='col-md-6 col-xs-12'>
	   <p>Название сертификата: 
			<input type='text' placeholder='Введите название...' name='ru_name' class='form-control' aria-describedby='sizing-addon2'>
		</p> 
		<p>Name of certificate: 
			<input type='text' name='en_name' placeholder='Write name certificate...' class='form-control' aria-describedby='sizing-addon2'>
		</p>
		</div>
		<div class='col-md-6 col-xs-12'>
        <p>Фотография сертификата: 
				<input type='file' name='img_certificate' class='form-control' aria-describedby='sizing-addon2' accept='image/*,image/jpeg'>
		</p>
		</div>&nbsp;
        
		
		<p>Опубликовать: <input type='checkbox' name='status' value='1'></p>
        <button class='btn btn-secondary' type='submit' value='Добавить' name='submit' onClick= 'return window.confirm(`Добавить сертификат??`);'>Добавить</button>
        </form>
        </div>";
		return $temp;
    }
	
	 static function add_submit()
    {
        $ru_name = $_POST['ru_name'];
        
		$en_name = $_POST['en_name'];
		
        
        $ru_url = CMS::translit($ru_name);
        $en_url = CMS::translit($en_name);
		
		$status = $_POST['status'];
        
        global $DB;
        $sql = $DB->prepare("INSERT INTO certificates (ru_name, en_name, status, ru_url, en_url) VALUES (?,?,?,?,?)");
        if(!$sql) echo($DB->error);
		$sql->bind_param('sssss', $ru_name, $en_name, $status, $ru_url, $en_url);
        if(!$sql) echo($DB->error);
		$sql->execute();
        $result = $sql->get_result();
		
		if($_FILES["img_certificate"]["size"]>0) 
		{
			$uploaddir = '../img-data/uploads/';
			if($_FILES["img_certificate"]["error"] == 0)
			{
				if($_FILES['img_certificate']['type'] == "image/gif" || $_FILES['img_certificate']['type'] == "image/jpg" || $_FILES['img_certificate']['type'] == "image/jpeg" || $_FILES['img_certificate']['type'] == "image/png") 
				{
					$uploadfile = $uploaddir .time().'_'. basename($_FILES['img_certificate']['name']);
					if(move_uploaded_file($_FILES['img_certificate']['tmp_name'], $uploadfile)) {
						$inserted_id = $sql->insert_id;
						$sql = $DB->prepare("UPDATE certificates SET img_certificate=? WHERE id=?");
						$uploadfile_real = '/img-data/uploads/' .time().'_'. basename($_FILES['img_certificate']['name']);
						$sql->bind_param('si', $uploadfile_real, $inserted_id);
						$sql->execute();
						$result = $sql->get_result();
					}
				}
				else
				{
					$error='Недопустимый формат изображения';
				}
			}
			else {
				$error='Изображение не загружено. Попробуйте ещё раз';
			}	
		}
		
        header("Location:/admin/?module=certificates");
    }
    
	static function edit_form($id)
    {
        global $DB;
        $temp = "";
        $sql = $DB->prepare("SELECT * FROM certificates WHERE id=?");
        $sql->bind_param('s', $id);
        $sql->execute();
        $result = $sql->get_result();
        while ($row = $result->fetch_assoc()) 
        {
            $id = $row['id'];
            
            $ru_name = $row['ru_name'];
            
            $en_name = $row['en_name'];
            
            $status = $row['status'];
        }
        $temp = "<div class='forma'>
        <form action='/admin/?module=certificates&action=edit' method='POST' enctype='multipart/form-data'>
        <input type='hidden' name='id' value='$id'>
        <button type='button' class='btn btn-danger'><a href='/admin/?module=certificates&action=delete&id=$id' onClick= 'return window.confirm(`Вы уверены, что хотите сертификат?`);'><span>Удалить новость</span></a></button>
        <h3>Редактирование сертификата</h3><br />
		<h4><b>Страница на русском языке</b></h4>
        <div class='col-md-6 col-xs-12'>
		<p>Название новости: 
			<input type='text' name='ru_name' value='$ru_name' class='form-control' aria-describedby='sizing-addon2'>
		</p> 
		<p>Name of news: 
			<input type='text' name='en_name' value='$en_name' class='form-control' aria-describedby='sizing-addon2'>
		</p>
		</div>
		<div class='col-md-6 col-xs-12'>
        <p>Превью фотографии новости: 
				<input type='file' name='img_certificate' class='form-control' aria-describedby='sizing-addon2' accept='image/*,image/jpeg'>
		</p>
		</div>
		<br />&nbsp";
        if($status =='1')
        {
            $temp .= "<p>Опубликован: <input type='checkbox' name='status' value='1' checked></p>";
        }
        else
        {
            $temp .= "<p>Опубликован: <input type='checkbox' name='status' value='1'></p>";
        }
        $temp .= "<button class='btn btn-secondary' type='submit' value='Изменить' name='submit' onClick= 'return window.confirm(`Изменить сертификат??`);'>Изменить</button>
        </form>
        </div>";
        return $temp;
    }
	
	 static function edit_submit()
    { 
        $id = $_POST['id'];
            
            $ru_name = $_POST['ru_name'];
            
            $en_name = $_POST['en_name'];
              
            $status = $_POST['status'];
            
            $ru_url = CMS::translit($ru_name);
            $en_url = CMS::translit($en_name);
       
        global $DB;
        $sql = $DB->prepare("UPDATE certificates SET  ru_name=?, en_name=?, status=?, ru_url=?, en_url=? WHERE id=?");
        $sql->bind_param('ssssss', $ru_name, $en_name, $status, $ru_url, $en_url, $id);
        $sql->execute();
		//var_dump($DB);
        $result = $sql->get_result();
		
		if($_FILES["img_certificate"]["size"]>0) 
		{
			$uploaddir = '../img-data/uploads/';
			if($_FILES["img_certificate"]["error"] == 0)
			{
				if($_FILES['img_certificate']['type'] == "image/gif" || $_FILES['img_certificate']['type'] == "image/jpg" || $_FILES['img_certificate']['type'] == "image/jpeg" || $_FILES['img_certificate']['type'] == "image/png") 
				{
					$uploadfile = $uploaddir .time().'_'. basename($_FILES['img_certificate']['name']);
					if(move_uploaded_file($_FILES['img_certificate']['tmp_name'], $uploadfile)) {
						$sql = $DB->prepare("UPDATE certificates SET img_certificate=? WHERE id=?");
						
						$uploadfile_real = '/img-data/uploads/' .time().'_'. basename($_FILES['img_certificate']['name']);
						
						$sql->bind_param('si', $uploadfile_real, $id);
						$sql->execute();
						$result = $sql->get_result();
					}
				}
				else
				{
					$error='Недопустимый формат изображения';
				}
			}
			else {
				$error='Изображение не загружено. Попробуйте ещё раз';
			}	
		}
		
        header("Location:/admin/?module=certificates");
    }
    
    static function delete($id)
    {
        global $DB;
        $sql = $DB->prepare("DELETE FROM certificates WHERE id=?");
        $sql->bind_param('s', $id);
        $sql->execute();
        header("Location:/admin/?module=certificates");
    }	
}

#####################################################################
//Модуль новостей
#####################################################################
class admin_news{
	
	static function get_list()
    {
        global $DB;
        $temp = "<button type='button' class='btn btn-success'><i class='fa fa-plus-circle' aria-hidden='true' style='margin-right:15px;'></i><a href='/admin/?module=news&action=add'>Добавить новость</a></button>
		<table class='table table-bordered'>
        <thead class='thead-dark'>
        <tr>
		<th>id</th>
		<th>Название новости</th>
		<th>Краткое описание</th>
		<th>Дата публикации</th>
		<th>Статус публикации</th>
        </tr>
        </thead>";
        $sql = $DB->prepare("SELECT id, ru_name as name, ru_mini_description as mini_description, ru_date as date, status FROM news");
        $sql->execute();
        $result = $sql->get_result();
        while ($row = $result->fetch_assoc()) 
        {
			//изменение стиля в зависимости от параметра
			$class = '';
			if($row['status'] == 1)
			{
				$class="admin_published";
			}
			else{
				$class="admin_published1";
			}
			
            if($row['status'] == 1)
            {
                $row['status'] = 'Опубликована';
            }
            else
            {
                $row['status'] = 'Не опубликована';
            }
            $temp .= "<tr class='".$class."' onclick='window.location.href=`/admin/?module=news&action=edit&id=$row[id]`; return false'>
						<td>$row[id]</td>
						<td>$row[name]</td>
						<td>$row[mini_description]</td>
						<td>$row[date]</td>
						<td>$row[status]</td>
					</tr>";
        }
        $sql->close();
        $temp .= "</table>";
        return $temp;
		return "";
    }

	static function add_form()
    {
        $temp = "<div class='forma'>
        <form action='/admin/?module=news&action=add' method='POST' enctype='multipart/form-data'>
        <h3>Добавление новой новости</h3><br />
		<h4><b>Страница на русском языке</b></h4>
		<div class='col-md-6 col-xs-12'>			
		<p>Название новости: 
			<input type='text' placeholder='Введите название...' name='ru_name' class='form-control' aria-describedby='sizing-addon2'>
		</p>
	   <p>Мини-описание: 
			<input type='text' name='ru_mini_description' placeholder='Введите страну' class='form-control' aria-describedby='sizing-addon2'>
		</p></div>
		<div class='col-md-6 col-xs-12'>
        <div class='block'>
            <p><b>TITLE: </b>
				<input type='text' name='ru_title' placeholder='Введите заголовок...' class='form-control' aria-describedby='sizing-addon2'>
			</p>
            <p><b>KEYWORDS: </b>
				<input type='text' name='ru_keywords' placeholder='Введите ключевые слова...' class='form-control' aria-describedby='sizing-addon2'>
			</p>
            <p><b>DESCRIPTION: </b>
				<input type='text' name='ru_description' placeholder='Введите описане...' class='form-control' aria-describedby='sizing-addon2'>
			</p>
        </div></div>
		<p>Дата публикации: 
			<input type='date' name='ru_date'  class='form-control' aria-describedby='sizing-addon2'>
		</p> 
		 <div class='col-md-6 col-xs-12'>
		<p>Превью фотографии новости: 
				<input type='file' name='image' class='form-control' aria-describedby='sizing-addon2' accept='image/*,image/jpeg'>
		</p>
		</div>
		<div class='col-md-6 col-xs-12'>
		<p>Фотография новости: 
				<input type='file' name='image_big' class='form-control' aria-describedby='sizing-addon2' accept='image/*,image/jpeg'>
		</p>
		</div>
		<p>Контент новости: <textarea id='ru_content_editor' name='ru_content' cols='100' rows='20'></textarea>
        <script type='text/javascript'>
            var ckeditor1 = CKEDITOR.replace( 'ru_content_editor' );
            AjexFileManager.init({
                returnTo: 'ckeditor',
                editor: ckeditor1
            });
        </script></p>
 		<br />
        
        <h4><b>Страница на английском языке</b></h4>
		 <p>Name of news: 
			<input type='text' placeholder='Write name...' name='en_name' class='form-control' aria-describedby='sizing-addon2'>
		</p> 
		<div class='col-md-6 col-xs-12'>
	   <p>Mini-description: 
			<input type='text' name='en_mini_description' placeholder='Write name news...' class='form-control' aria-describedby='sizing-addon2'>
		</p> 
		</div>
		<div class='col-md-6 col-xs-12'>
        <div class='block'>
            <p><b>TITLE: </b>
				<input type='text' name='en_title' placeholder='Write title...' class='form-control' aria-describedby='sizing-addon2'>
			</p>
            <p><b>KEYWORDS: </b>
				<input type='text' name='en_keywords' placeholder='Write keywords...' class='form-control' aria-describedby='sizing-addon2'>
			</p>
            <p><b>DESCRIPTION: </b>
				<input type='text' name='en_description' placeholder='Write description...' class='form-control' aria-describedby='sizing-addon2'>
			</p>
        </div></div>
		<p>Date of published: 
			<input type='date' name='ru_date'  class='form-control' aria-describedby='sizing-addon2'>
		</p> 
		<p>Another information: <textarea id='en_content_editor' name='en_content' cols='100' rows='20'></textarea>
        <script type='text/javascript'>
            var ckeditor1 = CKEDITOR.replace( 'en_content_editor' );
            AjexFileManager.init({
                returnTo: 'ckeditor',
                editor: ckeditor1
            });
        </script></p>
 		<br />
        
		<p>Опубликовать: <input type='checkbox' name='status' value='1'></p>
        <button class='btn btn-secondary' type='submit' value='Добавить' name='submit' onClick= 'return window.confirm(`Добавить новую новость??`);'>Добавить</button>
        </form>
        </div>";
		return $temp;
    }
	
	static function add_submit()
    {
		
		$ru_name = $_POST['ru_name'];
		$ru_mini_description = $_POST['ru_mini_description'];
		$ru_title = $_POST['ru_title'];
        $ru_keywords = $_POST['ru_keywords'];
        $ru_description = $_POST['ru_description'];
		$ru_date = $_POST['ru_date'];
        $ru_content = html_entity_decode($_POST['ru_content']);
        
        $en_name = $_POST['en_name'];
		$en_mini_description = $_POST['en_mini_description'];
		$en_title = $_POST['en_title'];
        $en_keywords = $_POST['en_keywords'];
        $en_description = $_POST['en_description'];
		$en_date = $_POST['en_date'];
        $en_content = html_entity_decode($_POST['en_content']);
        
        
		$ru_url = CMS::translit($ru_name);
        $en_url = CMS::translit($en_name);
        
		$status = $_POST['status'];
        
        global $DB;
        $sql = $DB->prepare("INSERT INTO news (ru_name, ru_mini_description, ru_title, ru_keywords, ru_description, ru_content, ru_date, en_name, en_title, en_keywords, en_description, en_date, en_mini_description, en_content, status, ru_url, en_url) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
        if(!$sql) echo($DB->error);
		$sql->bind_param('sssssssssssssssss', $ru_name, $ru_mini_description, $ru_title, $ru_keywords, $ru_description, $ru_content,$ru_date, $en_name, $en_title, $en_keywords, $en_description,$en_date, $en_mini_description, $en_content, $status, $ru_url, $en_url);
        $sql->execute();
        $result = $sql->get_result();
		
		if($_FILES["image"]["size"]>0) 
		{
			$uploaddir = '../img-data/uploads/';
			if($_FILES["image"]["error"] == 0)
			{
				if($_FILES['image']['type'] == "image/gif" || $_FILES['image']['type'] == "image/jpg" || $_FILES['image']['type'] == "image/jpeg" || $_FILES['image']['type'] == "image/png") 
				{
					$uploadfile = $uploaddir .time().'_'. basename($_FILES['image']['name']);
					if(move_uploaded_file($_FILES['image']['tmp_name'], $uploadfile)) {
						$inserted_id = $sql->insert_id;
						$sql = $DB->prepare("UPDATE news SET image=? WHERE id=?");
						$uploadfile_real = '/img-data/uploads/' .time().'_'. basename($_FILES['image']['name']);
						$sql->bind_param('si', $uploadfile_real, $inserted_id);
						$sql->execute();
						$result = $sql->get_result();
					}
				}
				else
				{
					$error='Недопустимый формат изображения';
				}
			}
			else {
				$error='Изображение не загружено. Попробуйте ещё раз';
			}	
		}
		
		if($_FILES["image_big"]["size"]>0) 
		{
			$uploaddir = '../img-data/uploads/';
			if($_FILES["image_big"]["error"] == 0)
			{
				if($_FILES['image_big']['type'] == "image/gif" || $_FILES['image_big']['type'] == "image/jpg" || $_FILES['image_big']['type'] == "image/jpeg" || $_FILES['image_big']['type'] == "image/png") 
				{
					$uploadfile = $uploaddir .time().'_'. basename($_FILES['image_big']['name']);
					if(move_uploaded_file($_FILES['image_big']['tmp_name'], $uploadfile)) {
						$inserted_id = $sql->insert_id;
						$sql = $DB->prepare("UPDATE news SET image_big=? WHERE id=?");
						$uploadfile_real = '/img-data/uploads/' .time().'_'. basename($_FILES['image_big']['name']);
						$sql->bind_param('si', $uploadfile_real, $inserted_id);
						$sql->execute();
						$result = $sql->get_result();
					}
				}
				else
				{
					$error='Недопустимый формат изображения';
				}
			}
			else {
				$error='Изображение не загружено. Попробуйте ещё раз';
			}	
		}
       header("Location:/admin/?module=news");
    }	
	
	static function edit_form($id)
    {
        global $DB;
        $temp = "";
        $sql = $DB->prepare("SELECT * FROM news WHERE id=?");
        $sql->bind_param('s', $id);
        $sql->execute();
        $result = $sql->get_result();
        while ($row = $result->fetch_assoc()) 
        {
            $id = $row['id'];
            
			$ru_name = $row['ru_name'];
			$ru_mini_description = $row['ru_mini_description'];
			$ru_title = $row['ru_title'];
			$ru_keywords = $row['ru_keywords'];
			$ru_description = $row['ru_description'];
			$ru_date = $row['ru_date'];
			$ru_content = html_entity_decode($row['ru_content']);
          
            $en_name = $row['en_name'];
			$en_mini_description = $row['en_mini_description'];
			$en_title = $row['en_title'];
			$en_keywords = $row['en_keywords'];
			$en_description = $row['en_description'];
			$en_date = $row['en_date'];
			$en_content = html_entity_decode($row['en_content']);
			
            $status = $row['status'];
          
        }
        $temp = "<div class='forma'>
        <form action='/admin/?module=news&action=edit' method='POST' enctype='multipart/form-data'>
        <input type='hidden' name='id' value='$id'>
		  <button type='button' class='btn btn-danger'><a href='/admin/?module=news&action=delete&id=$id' onClick= 'return window.confirm(`Вы уверены, что хотите удалить новость?`);'><span>Удалить новость</span></a></button>
		<h3>Редактирование новости</h3><br />
		<br><h4><b>Страница на руссском языке</b></h4>
		<div class='col-md-6 col-xs-12'>
		<p>Название новости: 
			<input type='text' name='ru_name' value='$ru_name' class='form-control' aria-describedby='sizing-addon2'>
		</p>
		<p>Мини-описание: 
			<input type='text' name='ru_mini_description' value='$ru_mini_description' class='form-control' aria-describedby='sizing-addon2'>
		</p>
		</div>
		<div class='col-md-6 col-xs-12'>
        <div class='block'>
            <p><b>TITLE: </b>
				<input type='text' name='ru_title'  value='$ru_title' class='form-control' aria-describedby='sizing-addon2'>
			</p>
            <p><b>KEYWORDS: </b>
				<input type='text' name='ru_keywords'  value='$ru_keywords' class='form-control' aria-describedby='sizing-addon2'>
			</p>
            <p><b>DESCRIPTION: </b>
				<input type='text' name='ru_description'  value='$ru_description' class='form-control' aria-describedby='sizing-addon2'>
			</p>
        </div></div>
		 <p>Дата публикации: 
			<input type='date' name='ru_date' value='$ru_date' class='form-control' aria-describedby='sizing-addon2'>
		</p> 
		<div class='col-md-6 col-xs-12'>
		<p>Превью фотографии новости: 
				<input type='file' name='image' class='form-control' aria-describedby='sizing-addon2' accept='image/*,image/jpeg'>
		</p>
		</div>
		<div class='col-md-6 col-xs-12'>
		<p>Фотография новости: 
				<input type='file' name='image_big' class='form-control' aria-describedby='sizing-addon2' accept='image/*,image/jpeg'>
		</p>
		</div>
        <p>Контент новости: <textarea id='ru_content_editor' name='ru_content' cols='100' rows='20'>$ru_content</textarea>
        <script type='text/javascript'>
            var ckeditor1 = CKEDITOR.replace( 'ru_content_editor' );
            AjexFileManager.init({
                returnTo: 'ckeditor',
                editor: ckeditor1
            });
        </script></p>
 		<br /><br />
        
        <br><h4><b>Страница на английском языке</b></h4>
        <div class='col-md-6 col-xs-6'>
		<p>Name of news: 
			<input type='text' name='en_name' value='$en_name' class='form-control' aria-describedby='sizing-addon2'>
		</p> 
		<p>Mini-decription: 
			<input type='text' name='en_mini_description' value='$en_mini_description' class='form-control' aria-describedby='sizing-addon2'>
		</p>
		</div>
		<div class='col-md-6 col-xs-12'>
        <div class='block'>
            <p><b>Title: </b>
				<input type='text' name='en_title' value='$en_title' class='form-control' aria-describedby='sizing-addon2'>
			</p>
            <p><b>Keywords: </b>
				<input type='text' name='en_keywords'  value='$en_keywords' class='form-control' aria-describedby='sizing-addon2'>
			</p>
            <p><b>Description: </b>
				<input type='text' name='en_description' p value='$en_description' class='form-control' aria-describedby='sizing-addon2'>
			</p>
        </div>
		</div>
		<p>Date of published: 
			<input type='date' name='en_date' value='$en_date' class='form-control' aria-describedby='sizing-addon2'>
		</p>
        <p>Another information: <textarea id='en_content_editor' name='en_content' cols='100' rows='20'>$en_content</textarea>
        <script type='text/javascript'>
            var ckeditor1 = CKEDITOR.replace( 'en_content_editor' );
            AjexFileManager.init({
                returnTo: 'ckeditor',
                editor: ckeditor1
            });
        </script></p>
		&nbsp;";
       if($status =='1')
        {
            $temp .= "<p>Опубликовать: <input type='checkbox' name='status' value='1' checked></p>";
        }
        else
        {
            $temp .= "<p>Опубликовано: <input type='checkbox' name='status' value='1'></p>";
        }
        $temp .= "
		<button class='btn btn-secondary' type='submit' value='Изменить' name='submit' onClick= 'return window.confirm(`Вы уверены, что хотите применить изменения к этой странице?`);'>Изменить</button>
        </form>
        </div>";
        return $temp;
    }
	
	static function edit_submit()
    { 
        $id = $_POST['id'];
            
			$ru_name = $_POST['ru_name'];
			$ru_mini_description = $_POST['ru_mini_description'];
			$ru_title = $_POST['ru_title'];
			$ru_keywords = $_POST['ru_keywords'];
			$ru_description = $_POST['ru_description'];
			$ru_date = $_POST['ru_date'];
			$ru_content = html_entity_decode($_POST['ru_content']);
          
            $en_name = $_POST['en_name'];
			$en_mini_description = $_POST['en_mini_description'];
			$en_title = $_POST['en_title'];
			$en_keywords = $_POST['en_keywords'];
			$en_description = $_POST['en_description'];
			$en_date = $_POST['en_date'];
			$en_content = html_entity_decode($_POST['en_content']);
			  
            $status = $_POST['status'];
			
			$ru_url = CMS::translit($ru_name);
            $en_url = CMS::translit($en_name);
            
            
        global $DB;
        $sql = $DB->prepare("UPDATE news SET ru_name=?, ru_title=?, ru_keywords=?, ru_description=?, ru_date=?, ru_mini_description=?, ru_content=?, en_name=?, en_title=?,  en_keywords=?, en_description=?, en_date=?, en_mini_description=?, en_content=?, status=?, ru_url=?, en_url=? WHERE id=?");
         if(!$sql) echo($DB->error);
		$sql->bind_param('ssssssssssssssssss', $ru_name, $ru_title,  $ru_keywords, $ru_description, $ru_date, $ru_mini_description, $ru_content,$en_name, $en_title, $en_keywords, $en_description, $en_date, $en_mini_description, $en_content, $status, $ru_url, $en_url, $id);
        $sql->execute();
        $result = $sql->get_result();
		if($_FILES["image"]["size"]>0) 
		{
			$uploaddir = '../img-data/uploads/';
			if($_FILES["image"]["error"] == 0)
			{
				if($_FILES['image']['type'] == "image/gif" || $_FILES['image']['type'] == "image/jpg" || $_FILES['image']['type'] == "image/jpeg" || $_FILES['image']['type'] == "image/png") 
				{
					$uploadfile = $uploaddir .time().'_'. basename($_FILES['image']['name']);
					if(move_uploaded_file($_FILES['image']['tmp_name'], $uploadfile)) {
						$sql = $DB->prepare("UPDATE news SET image=? WHERE id=?");
						
						$uploadfile_real = '/img-data/uploads/' .time().'_'. basename($_FILES['image']['name']);
						
						$sql->bind_param('si', $uploadfile_real, $id);
						$sql->execute();
						$result = $sql->get_result();
					}
				}
				else
				{
					$error='Недопустимый формат изображения';
				}
			}
			else {
				$error='Изображение не загружено. Попробуйте ещё раз';
			}	
		}
		
		if($_FILES["image_big"]["size"]>0) 
		{
			$uploaddir = '../img-data/uploads/';
			if($_FILES["image_big"]["error"] == 0)
			{
				if($_FILES['image_big']['type'] == "image/gif" || $_FILES['image_big']['type'] == "image/jpg" || $_FILES['image_big']['type'] == "image/jpeg" || $_FILES['image_big']['type'] == "image/png") 
				{
					$uploadfile = $uploaddir .time().'_'. basename($_FILES['image_big']['name']);
					if(move_uploaded_file($_FILES['image_big']['tmp_name'], $uploadfile)) {
						$sql = $DB->prepare("UPDATE news SET image_big=? WHERE id=?");
						
						$uploadfile_real = '/img-data/uploads/' .time().'_'. basename($_FILES['image_big']['name']);
						
						$sql->bind_param('si', $uploadfile_real, $id);
						$sql->execute();
						$result = $sql->get_result();
					}
				}
				else
				{
					$error='Недопустимый формат изображения';
				}
			}
			else {
				$error='Изображение не загружено. Попробуйте ещё раз';
			}	
		}
		
     header("Location:/admin/?module=news");
    }
	
	 static function delete($id)
    {
        global $DB;
        $sql = $DB->prepare("DELETE FROM news WHERE id=?");
        $sql->bind_param('s', $id);
        $sql->execute();
        header("Location:/admin/?module=news");
    }	
	
	
}




class admin_menu
{
    static function get()
    {
		global $_GET;
        $temp = '';
		if(!isset($_GET['module']))
			$_GET['module'] = 'pages';
		$class = ''; if($_GET['module'] == 'pages') $class = 'active';
        $temp .= '<a href="/admin/?module=pages" class="sl-menu-link '.$class.'"><div class="sl-menu-item"><i class="fa fa-file-text" aria-hidden="true"></i><span class="menu-item-label">Страницы сайта</span></div></a>';
		
		$class = ''; if($_GET['module'] == 'products') $class = 'active';
		$temp .= '<a href="/admin/?module=products" class="sl-menu-link '.$class.'"><div class="sl-menu-item"><i class="fa fa-cubes" aria-hidden="true"></i><span class="menu-item-label">Огнеупорные изделия</span></div></a>';
		
		$class = ''; if($_GET['module'] == 'sections') $class = 'active';
		$temp .= '<a href="/admin/?module=sections" class="sl-menu-link '.$class.'"><div class="sl-menu-item"><i class="fa fa-th-list" aria-hidden="true"></i><span class="menu-item-label">Категории продукции</span></div></a>';
		
		$class = ''; if($_GET['module'] == 'partners') $class = 'active';
		$temp .= '<a href="/admin/?module=partners" class="sl-menu-link '.$class.'"><div class="sl-menu-item"><i class="fa fa-users" aria-hidden="true"></i><span class="menu-item-label">Партнеры</span></div></a>';
		
		$class = ''; if($_GET['module'] == 'news') $class = 'active';
		$temp .= '<a href="/admin/?module=news" class="sl-menu-link '.$class.'"><div class="sl-menu-item"><i class="fa fa-newspaper-o" aria-hidden="true"></i><span class="menu-item-label">Новости</span></div></a>';
		
		$class = ''; if($_GET['module'] == 'company') $class = 'active';
		$temp .= '<a href="/admin/?module=company" class="sl-menu-link '.$class.'">
          <div class="sl-menu-item">
            <i class="fa fa-building" aria-hidden="true"></i>
            <span class="menu-item-label">Предприятие</span>
            <i class="menu-item-arrow fa fa-angle-down"></i>
          </div>
        </a>
        <ul class="sl-menu-sub nav flex-column">
		  <li class="nav-item"><a href="/admin/?module=admin_about_company" class="nav-link">О предприятии</a></li>
          <li class="nav-item"><a href="/admin/?module=history" class="nav-link">История предприятия</a></li>
          <li class="nav-item"><a href="/admin/?module=leadership" class="nav-link">Руководство</a></li>
        </ul>';
		
		$class = ''; if($_GET['module'] == 'contacts') $class = 'active';
		$temp .= '<a href="/admin/?module=contacts" class="sl-menu-link '.$class.'"><div class="sl-menu-item"><i class="fa fa-location-arrow" aria-hidden="true"></i><span class="menu-item-label">Контакты</span></div></a>';
		
		$class = ''; if($_GET['module'] == 'certificates') $class = 'active';
		$temp .= '<a href="/admin/?module=certificates" class="sl-menu-link '.$class.'"><div class="sl-menu-item"><i class="fa fa-file-image-o" aria-hidden="true"></i><span class="menu-item-label">Сертификаты</span></div></a>';
		
		$class = ''; if($_GET['module'] == 'zayavka_phone') $class = 'active';
		$temp .= '<a href="/admin/?module=zayavka_phone" class="sl-menu-link '.$class.'"><div class="sl-menu-item"><i class="fa fa-shopping-basket" aria-hidden="true"></i><span class="menu-item-label">Заказ продукции</span></div></a>';
		
		$class = ''; if($_GET['module'] == 'zayavka_buy') $class = 'active';
		$temp .= '<a href="/admin/?module=zayavka_buy" class="sl-menu-link '.$class.'"><div class="sl-menu-item"><i class="fa fa-phone" aria-hidden="true"></i><span class="menu-item-label">Заявка на звонок</span></div></a>';
		
		
//$temp .= '<li class="current"><a href="/admin/?module=pages"><i class="glyphicon glyphicon-file"></i>Страницы</a></li>';

        echo $temp;
    }
}
?>