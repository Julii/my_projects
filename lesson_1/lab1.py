'''загружаем библиотеку opencv'''
import cv2
print(cv2.__version__)

'''загружаем изображение и отображаем его'''
img = cv2.imread('img.jpg', 0)
print(img)
cv2.imshow('image', img)

k = cv2.waitKey(0)

if k == 27:
    cv2.destroyAllWindows()
elif k == ord('s'):
    cv2.imwrite('img_copy.jpg', img)
    cv2.destroyAllWindows()

