import cv2
#ПЕРВАЯ КАРТИНКА
pic1 = cv2.imread('picture1.jpg', 1)
#диагональная линия
cv2.line(pic1,(0,0), (500, 500), (222,229, 237), thickness =5, lineType= 8)
#прямоугольни
cv2.rectangle(pic1, (180,210), (470, 128), (2,255,0), -1)
#круг
cv2.circle(pic1, (90, 400), 50, (220, 107), -1)
#текст
cv2.putText(pic1, 'Example 1', (120,60), cv2.FONT_HERSHEY_TRIPLEX, 1.5, (0,0,0))
#стрелка
cv2.arrowedLine(pic1, (300,450), (420,450), (0,0,255), 2)

#ВТОРАЯ КАРТИНКА
pic2 = cv2.imread('picture2.jpg', 1)

cv2.namedWindow('image2', cv2.WINDOW_AUTOSIZE)

cv2.line(pic2, (40,85), (530,85), (150, 27, 222), thickness =3, lineType =8, shift=0)
cv2.rectangle(pic2, (200,200), (400, 150), (133, 230, 157), thickness = -1)
cv2.circle(pic2, (90, 200), 65, (220, 235, 148), -1)
cv2.putText(pic2, 'Example 2', (230,180), cv2.FONT_HERSHEY_COMPLEX_SMALL, 1.0, (237, 100, 141))
cv2.arrowedLine(pic2, (50,400), (300,210), (30, 129, 168), 5)




#ТРЕТЬЯ КАРТИНКА
pic3 = cv2.imread('picture3.jpg', 1)
cv2.line(pic3, (40,85), (530,85), (217, 195,130), thickness =3, lineType =8, shift=0)
cv2.rectangle(pic3, (200,200), (400, 150), (222, 38, 56), thickness = -1)
cv2.circle(pic3, (90, 200), 65, (38, 222, 203), -1)
cv2.putText(pic3, 'Example 3', (280,250), cv2.FONT_HERSHEY_COMPLEX_SMALL, 1.0, (242, 237, 238))
cv2.arrowedLine(pic3, (130,100), (150,170), (232, 195, 28), 2)



print(pic1)
print(pic2)
print(pic3)

cv2.imshow('image1', pic1)
cv2.imshow('image2', pic2)
cv2.imshow('image3', pic3)

k = cv2.waitKey(0)
if k == 27:
    cv2.destroyAllWindows()
elif k == ord('a'):
    cv2.destroyAllWindows()





