import numpy as np
import cv2

im_random = np.random.randint(255, size=(512, 512, 3), dtype=np.uint8)
cv2.imshow('Random Generator', im_random)
cv2.waitKey(0) & 0xFF
cv2.destroyAllWindows()