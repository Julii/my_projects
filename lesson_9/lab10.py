import cv2
import numpy as np
import dlib
from math import hypot

#загружаем изображение с камеры и создаем маску
cap = cv2.VideoCapture(0)
lips_image = cv2.imread("lips.png")
_, frame = cap.read()
rows, cols, _ = frame.shape
nose_mask = np.zeros((rows, cols), np.uint8)

#загружаем детектор лица
detector = dlib.get_frontal_face_detector()
predictor = dlib.shape_predictor("123.dat")

#получаем кадры с камеры и определяем ориентиры лица
while True:
    _, frame = cap.read()
    gray_frame = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)

    faces = detector(frame)
    for face in faces:
        landmarks = predictor(gray_frame, face)

#координаты губ
        top_lips = (landmarks.part(50).x, landmarks.part(50).y)
        center_lips = (landmarks.part(66).x, landmarks.part(66).y)
        left_lips = (landmarks.part(48).x, landmarks.part(48).y)
        right_lips = (landmarks.part(54).x, landmarks.part(54).y)
        lips_width = int(hypot(left_lips[0] - right_lips[0],
                           left_lips[1] - right_lips[1]))
        lips_height = int(lips_width * 0.77)

#новое расположение губ
        top_left = (int(center_lips[0] - lips_width / 2),
                              int(center_lips[1] - lips_height / 2))
        bottom_right = (int(center_lips[0] + lips_width / 2),
                            int(center_lips[1] + lips_height / 2))
        # cv2.circle(frame, top_nose, 3, (255, 0, 0), -1)

#создаем "новые" губы (заменяем нос с камеры на картинку)
        lips_face = cv2.resize(lips_image, (lips_width, lips_height ))
        lips_face_gray = cv2.cvtColor(lips_face, cv2.COLOR_BGR2GRAY)
        _, lips_mask = cv2.threshold(lips_face_gray, 25, 255, cv2.THRESH_BINARY_INV)

        lips_area = frame[top_left[1]: top_left[1] + lips_height,
                    top_left[0]: top_left[0] + lips_width]
        lips_area_no_lips = cv2.bitwise_and(lips_area, lips_area, mask = lips_mask)
        final_lips = cv2.add(lips_area_no_lips, lips_face)

        frame[top_left[1]: top_left[1] + lips_height,
                    top_left[0]: top_left[0] + lips_width] = final_lips


    #cv2.imshow("Nose area", nose_area)
    #cv2.imshow("Nose pig", nose_pig)
    #cv2.imshow("Final nose", final_nose)

    cv2.imshow("Frame", frame)


    key = cv2.waitKey(1)
    if key ==27:
        break